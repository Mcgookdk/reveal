//
//  UILookAndFeel.swift
//  Reveal
//
// As the global settings for Navigation bars etc don't work. This is where colours and other  look and feel components should be drawn from
//
//
//  Created by David McGookin on 07/04/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import Foundation
import UIKit

class UILookAndFeel{
    
    class func defaultTintColour() -> UIColor {
        return  UIColor(colorLiteralRed: 0.849, green: 0.272, blue: 0.024, alpha: 1.00)
        
    }
    
    class func defaultTitleTextColour() -> UIColor{
        return UIColor.white
    }
    
    
    class func defaultBackgroundColor() -> UIColor{
        return UIColor.white
    }
    
    
    
}
