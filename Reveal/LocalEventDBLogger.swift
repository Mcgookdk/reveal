//
//  LocalEventDBLogger.swift
//  Reveal
//  Acts as a local cache to log DB data to SQL Lite and then later upload to the server
//  Created by David McGookin on 03/05/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import Foundation




class LocalEventDBLogger: NSObject{


    static let FAILED_TO_INSERT  = -1; //The data failed to be inserted into the db
   
    static let ALL_RESULTS = -1; //Used to denote there should be no limit on the number of cached results returned. Be careful using this

    
    //Some constants for the titles of things in the db
    
    static let ID_STR = "id"
    static let FORMATTED_TIMESTAMP_STR = "formattedTimeStamp"
    static let TIMESTAMP_STR = "timeStampMS"
    static let EVENT_CODE_STR = "eventCode"
    static let DESCRIPTION_STR = "description"
    static let DEVID_STR = "devID"
    static let OSVERSION_STR = "osVersion"
    static let TOTAL_IMAGES_STR = "totalImages"
    static let GEO_IMAGES_STR = "geoImages"
    
    
    
    override init(){
        //open the db connection
        
}

    
    
    
    func logLocalEvent(formattedDate: String, dateSince1970: String, eventCode: Int, description: String) -> Int{
       
         let db = SQLiteDB.shared
        let result = db.execute(sql:"INSERT INTO LocalEvents VALUES (null, '\(formattedDate)', '\(dateSince1970)', \(eventCode), '\(description)')")
    
        if (result == 0){
            print ("insertion of Local event failed !!!!")
            return LocalEventDBLogger.FAILED_TO_INSERT
        }else{
            
            let entryID = db.query(sql: "SELECT MAX(id) from LocalEvents")
            
            if let maxEntryID = entryID[0]["MAX(id)"] as? Int{
                return maxEntryID
            }else{
                return LocalEventDBLogger.FAILED_TO_INSERT
            }
           
        }
    }
    
    
    func removeLoggedLocalEvent (primaryKey: Int){
        let db = SQLiteDB.shared
        let result = db.execute(sql:"DELETE  FROM LocalEvents WHERE id = \(primaryKey)")
        if (result == 0){
            print ("removal of Local Event  failed")
        }else{
            print ("removal of Local Event worked")
        }
    }
    
    func retrieveLoggedLocalDeviceStats(maxNumber: Int) -> [[String: Any]]{
        let db = SQLiteDB.shared
        var queryStr = "SELECT * FROM LocalDeviceStats"
        if(maxNumber != LocalEventDBLogger.ALL_RESULTS){
            queryStr = "\(queryStr) LIMIT \(maxNumber)"
        }
        let result = db.query(sql:queryStr)
        print("Query Result for Query \(queryStr) is \(result)");
        return result
    }

    
    
    func retrieveLoggedLocalEvents(maxNumber: Int) -> [[String: Any]]{
        let db = SQLiteDB.shared
        var queryStr = "SELECT * FROM LocalEvents"
        if(maxNumber != LocalEventDBLogger.ALL_RESULTS){
            queryStr = "\(queryStr) LIMIT \(maxNumber)"
        }
        let result = db.query(sql:queryStr)
        print("Query Result for Query \(queryStr) is \(result)");
        return result
    }
    
    
    
    
    
    func removeLoggedDevStat (primaryKey: Int){
        let db = SQLiteDB.shared
        let result = db.execute(sql:"DELETE  FROM LocalDeviceStats WHERE id = \(primaryKey)")
        if (result == 0){
            print ("removal of Local Dev Stat failed")
        }else{
            print ("removal of Local Dev Stat worked")
        }
    }
    
    func logLocalDevStat(formattedDate: String, dateSince1970: String, devID: String, osVersion: String, totalImages: Int, geoImages: Int) -> Int{
        
         let db = SQLiteDB.shared
        let result = db.execute(sql:"INSERT INTO LocalDeviceStats VALUES (null, '\(formattedDate)', '\(dateSince1970)', '\(devID)', '\(osVersion)', \(totalImages), \(geoImages))")
        
        if (result == 0){
            print ("insertion of Local dev stat  failed !!!!")
            return LocalEventDBLogger.FAILED_TO_INSERT
        }else{
             let entryID = db.query(sql: "SELECT MAX(id) from LocalDeviceStats");
            if let maxEntryID = entryID[0]["MAX(id)"] as? Int{
                return maxEntryID
            }else{
                return LocalEventDBLogger.FAILED_TO_INSERT
            }
            
        }
    }
    
    //returns the number of entries in the DB for local events
    func outstandingLocalEvents() -> Int{
        
         let db = SQLiteDB.shared
       let result =  db.query(sql: "SELECT COUNT (id) FROM LocalEvents")
        print("Found \(result) records in the local event table")
        if let noItem = result[0]["COUNT (id)"] as? Int{
            return noItem
        }else{
            return 0
        }
      
    }
    
    func outstandingLocalDevStats() -> Int{
         let db = SQLiteDB.shared
        let result = db.query(sql: "SELECT COUNT (id) FROM LocalDeviceStats")
        print("Found \(result) records in the local dev stat table")
        if let noItem = result[0]["COUNT (id)"] as? Int{
            return noItem
        }else{
            return 0
        }
    }

}
