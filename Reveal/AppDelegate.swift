//
//  AppDelegate.swift
//  Reveal
//
//  Created by David McGookin on 16/02/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications
import Foundation




//Uncomment out this print statement to allow for print to console for Debuging
func print(_ items: Any..., separator: String = " ", terminator: String = "\n"){}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, EncapsulatedLocationManagerDelegate, StatusViewControllerDelegate, PhotoNotificationManagerDelegate {
    
    
    // a static constant for the participant identifier if it is invlaid and needs to be set!
    let INVALID_PARTICIPANT_ID = "INVALID_IDENTIFIER"

    var window: UIWindow?
    
    //constants for location manager to avoid burning up the battery
    let DEFAULT_LOCATION_ACCURACY = kCLLocationAccuracyBestForNavigation;// kCLLocationAccuracyNearestTenMeters;
    let DEFAULT_MIN_LOCATION_MOVE = 30.0; //in meters
    
    let DEFAULT_DISTANCE_IN_METERS_BEYOND_SEARCH_RAD_TO_CLEAR_NOTIFICATION = 10.0;// If the user has moved the search radius and beyond since last notification we clear it
    let DEFAULT_MINIMUM_ACCEPTABLE_LOCATION_ACCURACY = 70.0
    let DEFAULT_MINIMUM_TIME_BETWEEN_USED_LOCATION_UPDATES_IN_SECONDS = 5.0 //even when we get good location updates, we don't wantthem too fast. Ignore anything within XX seconds of the last one
    
    let DEFAULT_NO_FIND_LOCATION_AUTO_CANCEL = 60.0 ; //60 seconds then we just turn off location updates
    
    let DEFAULT_DISTANCE_BETWEEN_SEARCHES = 200.0 //
    let DEFAULT_DISTANCE_BETWEEN_SEARCHES_IN_POOR_GPS_RECEPTION = 400.0 //We use this if the GPS repeatedly cannot get a good GPS location to increase the size of the geofence
    
    var mNumberOffailedHighPowerGPSSearches = 0 // We log the number of GPS searches that failed after NO FIND AUTO CANCEL
    let MAX_NUMBER_OF_FAILED_GPS_SEARCHES = 3 //After this number we increase the geofence size.
    
    
    var mForceImageSearch = false; //true iff the next image search should be "forced", i.e. ignore whatever we previously did and just search
    
    
    var mIsActiveNotification = false //Is there currently a notification for images
    var mNotificationIdentifierString = "NO NOTIFICATION"
    var mNotificationFireTime = Date()
    var mNotificationFireLocation = CLLocation(latitude: 0.0, longitude: 0.0)
    var mIsGeoFenceSet = false
    var mGeofenceID = "NO GEOFENCE ID"
    
    var mForegroundActiveTime = Date()
    var mNoificationFireManager = NotificationFireManager()
    
    var mHaveWrittenPhoneStats = false //We write out the stats of the device model, os etc once per app launch on the first result of the image search
    
    
    var mLastUserLocation = CLLocation(coordinate: CLLocationCoordinate2DMake(0.0, 0.0), altitude: 1, horizontalAccuracy: 1, verticalAccuracy: 1, timestamp: Date(timeIntervalSince1970: 0)) //Used to store the last processed user location. We need the full constructor to have a valid time stamp here
    var mLastSearchedUserLocation = CLLocation(latitude: 0.0, longitude: 0.0)
    var mLastPhotoSearchResults: PhotoManagerResult?
    
     var mPhotoNotificationManager = PhotoNotificationManager()
    
    var mStartTimeOfHighLocationUpdates: Date = Date();

    //root controllers
    var mTabBarController: UITabBarController?
    var mNavigationController: UINavigationController?
    
    //Status View Controller
    var mStatusViewController: StatusViewController?
    var mMainResultViewController: ImageResultViewController?
    
    //locationUpdate Stuff
    var mEncapsulatedLocationManager: EncapsulatedLocationManager?
    
    
    //   func setDefaultLookAndFeel(){
 
      //
  
        //Image Background Navigation Bar
       // let navBackgroundImage:UIImage! = UIImage(named: "navigation_bar.png")
        //UINavigationBar.appearance().setBackgroundImage(navBackgroundImage, for: .default)
      //  UIWindow.appearance().tintColor = UILookAndFeel.defaultTintColour()
       // UINavigationBar.appearance().backgroundColor = UILookAndFeel.defaultTintColour()
        //UINavigationBar.appearance().tintColor = UIColor.white
        ////ITabBar.appearance().backgroundColor = UIColor.white
       // UITabBar.appearance().tintColor = UILookAndFeel.defaultTintColour()
        
        
    //}
    
    func manualUpdateImageResults(){
        if(!mForceImageSearch){
        EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_REQUESTED_MANUAL_PHOTO_SEARCH, description: "User Requested Manual Photo Update Search at \(Date())")
        //set the force flag (so next time it is called we force the update
        mForceImageSearch = true;
        //set the location updates to high
        mEncapsulatedLocationManager?.foregroundLocationUpdates()
        }else{
            EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_REQUESTED_MANUAL_PHOTO_SEARCH_WHEN_ALREADY_DOING_SO, description: "USer tried to request manual photo search, but we were already doing that")
        }
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
         AppDelegate.dumpIt(text: "App Did Launch")
        
        
        
        //start the plist manager for prefs
        UIDevice.current.isBatteryMonitoringEnabled = true //setup battery monitoring
        
        
   
       mPhotoNotificationManager.authoriseNotifications() //We are the delegate that gets called
        
        
        //Do we have a participant id?
        
    
        let state = UIApplication.shared.applicationState
        if state == .background {
            EventLogger.sharedInstance.LogEvent(code: EventLogger.APP_WAS_LAUNCHED_IN_BACKGROUND, description: "Application Was Launched in Background ********")
        }else{
            EventLogger.sharedInstance.LogEvent(code: EventLogger.APP_WAS_LAUNCHED, description: "Application Was Launched ********")
        }
        
        PlistManager.sharedInstance.startPlistManager();
                //setup the navigation control hierarchy
        self.window = UIWindow(frame:UIScreen.main.bounds);
      
        
        //setup the default look and feel. Note must occur after the window is init
       // self.setDefaultLookAndFeel()
        self.window?.tintColor = UILookAndFeel.defaultTintColour()
        mTabBarController = UITabBarController();
        //mTabBarController?.tabBar.isTranslucent = false;        //set default look and feel
     //  mTabBarController?.tabBar.barTintColor = UIColor.black
     //   mTabBarController?.tabBar.tintColor = UIColor.white
        
       mMainResultViewController = ImageResultViewController()//MainViewController();
     
        //
       mMainResultViewController?.extendedLayoutIncludesOpaqueBars = true;
        mNavigationController = UINavigationController();
        mNavigationController?.view.backgroundColor = UILookAndFeel.defaultTitleTextColour(); //Some of the views we add might not extend to the the tab bar base, so we set the background of us to be white so it doesn't look odd.
    
         mNavigationController?.navigationBar.barTintColor =  UILookAndFeel.defaultTintColour()
        mNavigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UILookAndFeel.defaultTitleTextColour()]
        

       mNavigationController?.navigationBar.tintColor = UILookAndFeel.defaultTitleTextColour()
        mNavigationController?.pushViewController(mMainResultViewController!, animated: false);
        
        
        //setup the status view controller
      //  let navController = UINavigationController();
      //  navController.view.backgroundColor = UIColor.white;
       // navController.navigationBar.barTintColor =  self.window?.tintColor
       // navController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
       // navController.navigationBar.tintColor = UIColor.white
       // navController.tabBarItem = UITabBarItem(title: "Status", image: #imageLiteral(resourceName: "status_tab_icon"), selectedImage: nil);
       // mStatusViewController = StatusViewController()
       // mStatusViewController?.set(delegate: self)
        
      //  navController.pushViewController(mStatusViewController!, animated: false);
        
      //  mTabBarController?.setViewControllers([mNavigationController!, navController], animated: false);
        self.window?.rootViewController = mNavigationController;// mTabBarController;
        self.window?.makeKeyAndVisible();
        
        mPhotoNotificationManager.setDelegate(_delegate: self)
        
        let participantID = PlistManager.sharedInstance.getValueForKey(key: "ParticipantIdentifier") as? String
        if(participantID == INVALID_PARTICIPANT_ID){
            print("requesting User id")
            getParticipantID() // Get a participant id from the user
        }else{
            print("Have userid")
            EventLogger.sharedInstance.setUserID(userID: participantID!)
        }
        

        
        //setup the location manager
        mEncapsulatedLocationManager = EncapsulatedLocationManager(accuracy: DEFAULT_LOCATION_ACCURACY, minDistance: DEFAULT_MIN_LOCATION_MOVE, monitorSigChange: true, monitorBackgroundUpdates: true, delegate: self);
       
      mEncapsulatedLocationManager?.startLocationUpdates();
        mStartTimeOfHighLocationUpdates = Date()
        
        
        return true
        
    }
      //called to tidy up the notification parameters. Note this should be called in response to a notification that has already been cleared (e.g. user opened the app via a notification ,or opened the app and a notification is pending
    func clearNotification(){
        mPhotoNotificationManager.removeNotification(id: mNotificationIdentifierString)
        AppDelegate.dumpIt(text: "AD: Cancelled Notification. No valid notification currently: \(mNotificationIdentifierString)")
        mIsActiveNotification = false
         UIApplication.shared.applicationIconBadgeNumber = 0 //set the badge icon to 0
        mNotificationFireLocation = CLLocation(latitude: 0.0, longitude: 0.0)
    }
    
    //Encapsulated location Manager Delegate Methods
    func encapsulatedLocationManagerDidBackgroundLocationUpdates(_ manager: CLLocationManager){
        //get if we are running in the background or not
        let state = UIApplication.shared.applicationState
        if state == .background {
            AppDelegate.dumpIt(text: "AD: Location Manager Did Background Updates IN BACKGROUND")
        }else{
            AppDelegate.dumpIt(text: "AD: Location Manager Did background Updates IN FOREGROUND")
        }
        
        
    }
    
    func encapsulatedLocationManagerDidForegroundLocationUpdates(_ manager: CLLocationManager){
        //get if we are running in the background or not
        let state = UIApplication.shared.applicationState
        if state == .background {
            AppDelegate.dumpIt(text: "AD: Location Manager Did Foreground Updates IN BACKGROUND")
        }else{
            AppDelegate.dumpIt(text: "AD: Location Manager Did foreground Updates IN FOREGROUND")
        }
    }
    
    func encapsulatedLocationManagerDidUpdateLocations (locations: [CLLocation]){
        
        
        var shouldAbort = false; //Gotten a bit complex. Need a flag to decide if we should abort after setting the geofence
        
        //get if we are running in the background or not
        let state = UIApplication.shared.applicationState
        
        //if the accuracy level is not acceptable, we don't even consider using it.
        if (locations[0].horizontalAccuracy > DEFAULT_MINIMUM_ACCEPTABLE_LOCATION_ACCURACY){
            AppDelegate.dumpIt(text: "Location Accuracy was \(locations[0].horizontalAccuracy)m which is not good enough. We reject this update")
            print ("LU: Location Accuracy was \(locations[0].horizontalAccuracy)m which is not good enough. We reject this update")
            
            
            //If we have failed three times with the GPS on, then count it. 
            if(mEncapsulatedLocationManager?.isLocationForeground())!{
                if(Date().timeIntervalSince(mStartTimeOfHighLocationUpdates) > DEFAULT_NO_FIND_LOCATION_AUTO_CANCEL){
                mEncapsulatedLocationManager?.backgroundLocationUpdates()
                AppDelegate.dumpIt(text: "We have gone 60 secs without a good enough location. We are backgrounding location updates Timestamp: \(Date().timeIntervalSince(mStartTimeOfHighLocationUpdates))")
                mNumberOffailedHighPowerGPSSearches += 1
                
                }
            }
            
            shouldAbort = true
 
        }else{
            AppDelegate.dumpIt(text: "Location Accuracy was \(locations[0].horizontalAccuracy)m which is fine. We ACCEPT this update")
            print ("LU: Location Accuracy was \(locations[0].horizontalAccuracy)m which is fine . We ACCEPT this update")
            
        }
            
        
        
        if(locations[0].timestamp.timeIntervalSince(mLastUserLocation.timestamp) < DEFAULT_MINIMUM_TIME_BETWEEN_USED_LOCATION_UPDATES_IN_SECONDS && !mForceImageSearch){
            AppDelegate.dumpIt(text: "Not enough time since last location update has occured \(AppDelegate.getFormattedDate(date: mLastUserLocation.timestamp)). Ignoring this one \(AppDelegate.getFormattedDate(date: locations[0].timestamp))")
             print ("LU: Not enough time since last location update has occured \(mLastUserLocation.timestamp). Ignoring this one \(locations[0].timestamp)")
            shouldAbort = true
        }else{
            AppDelegate.dumpIt(text: "Enough time since last location update has occured \(AppDelegate.getFormattedDate(date: mLastUserLocation.timestamp)). USING this one \(AppDelegate.getFormattedDate(date: locations[0].timestamp))")
            print ("Enough time since last location update has occured \(AppDelegate.getFormattedDate(date: mLastUserLocation.timestamp)). USING this one \(AppDelegate.getFormattedDate(date: locations[0].timestamp))")
    
    }
    
        //if there is no geofence id set, then set a geofence here
        //Set it to be bigger if we are having a poor run of GPS locations
        let minDistanceBetweenSearches = getMinDistanceBetweenSearches()
        if(mIsGeoFenceSet == false){
            var geoFenceRad = minDistanceBetweenSearches
            if(mNumberOffailedHighPowerGPSSearches > MAX_NUMBER_OF_FAILED_GPS_SEARCHES){
                geoFenceRad = DEFAULT_DISTANCE_BETWEEN_SEARCHES_IN_POOR_GPS_RECEPTION
                mNumberOffailedHighPowerGPSSearches = 0 //Reset this
            }
            
                mGeofenceID =   (mEncapsulatedLocationManager?.applyExitGeoFence(location: locations[0], radius: geoFenceRad))!
                AppDelegate.dumpIt(text: "LU: Set Geo-Fence in UpdateLocations \(mGeofenceID) with radius \(geoFenceRad)m")
            mIsGeoFenceSet = true;
        }
            
        
        //if we should abort do it now. We either don't have accuracy or time between the location updates. So we don't want to use this update.
        if(shouldAbort == true){
            let msg = "We have an abort signal for the update, so are bailing out now"
            print(msg)
            AppDelegate.dumpIt(text: msg)
            return
        }else{
            let msg = "All is well and we don't need to abort"
            print(msg)
            AppDelegate.dumpIt(text: msg)
        }
  
     
        
        //Start to work through the options
        mLastUserLocation = locations[0];
        
        //If we shouldn't abort, we have a good enough signal so reset this counter
        mNumberOffailedHighPowerGPSSearches = 0 //Reset this
        //and turn off high power location updates 
        mEncapsulatedLocationManager?.backgroundLocationUpdates()
        
       
        
        //handle if there is an active notification
        //if we aren't too far away yet, then leave it otherwise remove the notification.
        if(mIsActiveNotification){
            let searchAreaSize = PlistManager.sharedInstance.getValueForKey(key: "SearchAreaSize") as? NSNumber
            if (mLastUserLocation.distance(from: mNotificationFireLocation) > ((searchAreaSize?.doubleValue)! + DEFAULT_DISTANCE_IN_METERS_BEYOND_SEARCH_RAD_TO_CLEAR_NOTIFICATION) || mForceImageSearch){
                clearNotification()
                EventLogger.sharedInstance.LogEvent(code: EventLogger.NOTIFICATION_AUTODISMISSED, description: "Notification Auto-dismissed")
                AppDelegate.dumpIt(text: "AD: Auto clearing notification as we walked far enough away: \(mNotificationIdentifierString)")
            }else{
                print("NOTIFICATION IS ACTIVE BUT WE ARE NOT TOO FAR AWAY YET. NO NEED TO FURTHER PROCESS LOCATION")
                AppDelegate.dumpIt(text: "AD: Notification is active and valid, so we don't process further: \(mNotificationIdentifierString)")
             //   if state == .background { //if we are in background make sure GPS is powered down
               //     mEncapsulatedLocationManager?.backgroundLocationUpdates()
                   // mEncapsulatedLocationManager?.stopLocationUpdates()
                //}
                
                return
            }
        }
        
        //There is no active notification, or it was just cleared
        //Now we can go search for photos!
        
        if((mLastUserLocation.distance(from: mLastSearchedUserLocation) > getMinDistanceBetweenSearches()) || mForceImageSearch){
           
            
            //check if we are in the background. If we are, then power down the GPS. This might not be the only place we need to do this
            //e.g. might need to consider the
            
            if(mForceImageSearch){
                mForceImageSearch = false; //We are doing it
            }
            
          //  if state == .background {
             //   mEncapsulatedLocationManager?.backgroundLocationUpdates()
              //  AppDelegate.dumpIt(text: "LU: In background with valid location object, stopping location updates")
              //  mEncapsulatedLocationManager?.stopLocationUpdates()
          //  }
           
            mLastSearchedUserLocation = mLastUserLocation //set this location as the last searched location
            
            print("Performing new search for images at location ")
            AppDelegate.dumpIt(text: "LU: Performing Image Search at location \(mLastSearchedUserLocation.coordinate) with Accuracy \(mLastSearchedUserLocation.horizontalAccuracy)")
            
  
            let searchAreaSize = PlistManager.sharedInstance.getValueForKey(key: "SearchAreaSize") as? NSNumber
              mLastPhotoSearchResults =   PhotoManager.searchImagesForLocation(location: mLastUserLocation, radius: (searchAreaSize?.doubleValue)!)
            
            
            //have we written the phone stats for this run out?
            if (!mHaveWrittenPhoneStats){
                mHaveWrittenPhoneStats = true
                writePhoneStats(imgResults: mLastPhotoSearchResults!)
            }
            
            //parese the results of the search
            let minNumberOfGeoImagesToFire = PlistManager.sharedInstance.getValueForKey(key: "ImagesRequiredForNotification") as? NSNumber
            if((mLastPhotoSearchResults?.numberOfUnseenGeoCodedImagesInRequestedArea())! >= (minNumberOfGeoImagesToFire?.intValue)!){//i.e. there is at least N, then fire then update. Note we make this on the UNSEEN geoimages in the area
                print("We found New geo-images")
                
                //We should request fire authorisation //NB we don't use this yet!
                  let shouldFire =  mNoificationFireManager.getFireAuthoristation() //Note we are just testing this here!
               
                
                if(shouldFire){
                AppDelegate.dumpIt(text: "LU: Searched and Found New Geo Images \(mLastPhotoSearchResults?.mGeoCodedImagesInRequestedArea). Search Area size was \(searchAreaSize)")
                EventLogger.sharedInstance.LogEvent(code: EventLogger.VALID_GEO_PHOTO_SEARCH_RESULT, description: "Found GeoPhotos: \(mLastPhotoSearchResults!.csvStrDescription())")
                mNotificationFireLocation = mLastUserLocation
                mMainResultViewController?.updateImageResult(result: mLastPhotoSearchResults!) // tell the view controller to update
                mNotificationIdentifierString = mPhotoNotificationManager.fireImageNearbyNotification(numberOfImages: (mLastPhotoSearchResults?.mGeoCodedImagesInRequestedArea)!)
                mNotificationFireTime = Date()
                AppDelegate.dumpIt(text: "LU: Fired Notification \(mNotificationIdentifierString)")
                EventLogger.sharedInstance.LogEvent(code: EventLogger.NOTIFICATION_ISSUED, description: "Notification was fired")
                if (mNotificationIdentifierString != PhotoNotificationManager.NOTIFICATION_POST_ERROR){
                    mIsActiveNotification = true
                }else{
                    print("NOTIFICATION FIRE ERROR")
                    AppDelegate.dumpIt(text: "Notification fire error")
                }
                }else{ // we did not get fire auth
                    let msg = "Found GeoPhotos but fire request refused \(mLastPhotoSearchResults!.csvStrDescription())"
                    EventLogger.sharedInstance.LogEvent(code: EventLogger.NOTIFICATION_REQUEST_REFUSED, description: msg)
                }
            }else{
                print("We did not find any geoimages")
                //we still supply this to the main view
                mMainResultViewController?.updateImageResult(result: mLastPhotoSearchResults!)
                EventLogger.sharedInstance.LogEvent(code: EventLogger.NO_GEO_PHOTO_SEARCH_RESULT, description: "Did Not find GeoPhotos: \(mLastPhotoSearchResults!.csvStrDescription())")
                AppDelegate.dumpIt(text: "LU: Searched area size:   \(searchAreaSize) and Found No New Geo Images. But updating result view controller with results")
            }
            
            
        }else{
                print("We have not moved far enough to perform a new search")
            AppDelegate.dumpIt(text: "LU: User moved \(mLastUserLocation.distance(from: mLastSearchedUserLocation)) meters.  Not far enough to perfom a new search")
            }
    
    }
    
    func encapsulatedLocationManager(_ manager: CLLocationManager, didFailWithError error: Error){}
    
    func encapsulatedLocationMangerAuthorisationDidFail(status: CLAuthorizationStatus){
        //the user denied or is not able to authorise location updates. We need to tell them what this means
        
        print ("DELEGATE LOC MAN FAIL");
        let alert = UIAlertController(title: "Error", message: "$(PRODUCT_NAME) needs location services enabled to work. Please change this in the settings", preferredStyle: UIAlertControllerStyle.alert);
        alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil));
        self.window?.rootViewController?.present(alert, animated: true, completion: nil);
    }
    
    //utility function to get min distance we should travel between searches of the photos
    func getMinDistanceBetweenSearches() -> Double{
        return DEFAULT_DISTANCE_BETWEEN_SEARCHES
            
            //Double((PlistManager.sharedInstance.getValueForKey(key: "SearchAreaSize") as? NSNumber)!) + DEFAULT_DISTANCE_IN_METERS_BEYOND_SEARCH_RAD_TO_CLEAR_NOTIFICATION //We need to travel beyond the search radius size plus a little
    }
    func encapsulatedLocationManagerUserDidExitRegion(id: String){
        
           AppDelegate.dumpIt(text: "BL: In Exit Region: Battery Level \(UIDevice.current.batteryLevel)")
          //  print("In Exit Region: Battery Level \(UIDevice.current.batteryLevel)")
        //  AppDelegate.dumpIt(text: "User Exited Region \(id)")
        //What is the current location and its accuracy ??
        let curLocation = mEncapsulatedLocationManager?.getLocation()
        AppDelegate.dumpIt(text: "RM: User Exited Region \(id) at location \(curLocation?.coordinate) with accuracy \(curLocation?.horizontalAccuracy)")
        
        mIsGeoFenceSet = false
        mGeofenceID = "NO VALID GEOFENCE ID"
        
        //setup a new geo-fence
    //   BEBUG OUT ON SUNDAY NIGHT
        //let  minDistanceBetweenSearches = getMinDistanceBetweenSearches()
     //   mGeofenceID =   (mEncapsulatedLocationManager?.applyExitGeoFence(location: mLastUserLocation, radius: minDistanceBetweenSearches))!
      //  mIsGeoFenceSet = true; // We need to reset this so more aren't added
       // AppDelegate.dumpIt(text: "RM: Set Geo-Fence \(mGeofenceID) in EXIT REGION with radius \(minDistanceBetweenSearches)m")
        
      
        mEncapsulatedLocationManager?.dumpCurrentRegions()
        //We might want here to know if the location services are being updated, and if not then start them again. OR we could use this to get location and call the update method?
        
        //Set the GPS to high power
        mEncapsulatedLocationManager?.foregroundLocationUpdates()
        mStartTimeOfHighLocationUpdates = Date() ;// init it here
    //  mEncapsulatedLocationManager?.resumeLocationUpdates() //THIS DOESNT WORK! We think (but we really arent sure)
    
       
        
        //we are calling the delegate instead. Seems to provide reasonable location accuracy
     //DKM DUBUG OUT SUNDAY NIGHT   self.encapsulatedLocationManagerDidUpdateLocations(locations: [curLocation!]);
        
       
    }
    //Notification Manager Delegate
    func appTriggeredFromNotification(withID:  String){
        
        //Cancel out our current notification!
        print("CANCELING THE NOTIFICATION!!!!")
       AppDelegate.dumpIt(text: "AD: Cancelling notification as app triggered from notification")
        clearNotification()
        UIApplication.shared.applicationIconBadgeNumber = 0 //set the badge icon to 0
    }
    
    
    //Status view controller Delegate
    func lastLocationUpdate()->CLLocation{
        return mLastUserLocation
    }
    
    func lastPhotoSearchLocation()->CLLocation{
        return mLastSearchedUserLocation
    }
    
    func photoSearchResultsForLastLocation()->PhotoManagerResult{
        return mLastPhotoSearchResults!
    }
    
    func lastNotificationTriggerTime()->Date{
        return mNotificationFireTime
    }
    
    //Notification Delegates
    
   
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        AppDelegate.dumpIt(text: "Application Resigning Active")
    }

    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
       // AppDelegate.dumpIt(text: "Application Did Enter background, powering down GPS")
      //  DEBUG OUT  mEncapsulatedLocationManager?.backgroundLocationUpdates()
     //   mEncapsulatedLocationManager?.pauseLocationUpdates()
        
        var appActiveTimeSeconds:Double = 0.0
        appActiveTimeSeconds = Date().timeIntervalSince(mForegroundActiveTime)
        
        EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_CLOSED_APP, description: "Application Entered Background after (seconds), \(appActiveTimeSeconds)")
    
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
      //  AppDelegate.dumpIt(text: "Application Will Enter Foreground, powering up GPS")
       // DEBUG OUT  mEncapsulatedLocationManager?.foregroundLocationUpdates()
       // mEncapsulatedLocationManager?.resumeLocationUpdates()
                EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_OPENED_APP, description: "Application Entered Foreground")
            AppDelegate.dumpIt(text: "Application will Enter Foreground was Called")
        UIApplication.shared.applicationIconBadgeNumber = 0 //Even if there is a notification, clear the badge icon
        mForegroundActiveTime = Date()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
                   //We are entering the foreground. We should check if there are any notifications and clear them!
       // AppDelegate.dumpIt(text: "Application Did Become Active")
        print("Entering the Foreground")
            if(mIsActiveNotification){
                clearNotification()
                AppDelegate.dumpIt(text: "Cancelling notification as app entering from foreground")
                print("Clearing active notification")
            }
        
        
        //Check and flush the log cache at this point. Not we should probably be checking for server connectivity here!
        EventLogger.sharedInstance.flushServerLogCache();
        
        
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        AppDelegate.dumpIt(text: "APP IS TERMINATING")
         EventLogger.sharedInstance.LogEvent(code: EventLogger.APP_WAS_TERMINATED, description: "Application Was Terminated ****************")
    }
    
    func getParticipantID(){
        let alert = UIAlertController(title: "Participant ID",
                                      message: "Please enter your participant ID",
                                      preferredStyle: .alert)
        // Submit button
        let submitAction = UIAlertAction(title: "Submit", style: .default, handler: { (action) -> Void in
            // Get 1st TextField's text
            let textField = alert.textFields![0]
            let pID = textField.text! as NSString
            PlistManager.sharedInstance.saveValue(value: pID, forKey: "ParticipantIdentifier")
            EventLogger.sharedInstance.setUserID(userID: textField.text!)
            
                print("Got NEW ID AS \(textField.text!)")
        })
    
        // Add 1 textField and customize it
        alert.addTextField { (textField: UITextField) in
            textField.keyboardAppearance = .dark
            textField.keyboardType = .default
            textField.autocorrectionType = .default
            textField.placeholder = "Type something here"
            textField.clearButtonMode = .whileEditing
        }
    
        // Add action buttons and present the Alert
        alert.addAction(submitAction)
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)

    }
    
    func writePhoneStats(imgResults: PhotoManagerResult){
        
        let model = UIDevice.current.modelName //this uses a swift extension UIDeviceExtension
        let osVersion = UIDevice.current.systemVersion
        EventLogger.sharedInstance.logPhoneStats(model: model, oSString: osVersion, images: imgResults.mImagesInLibrary, geoImages: imgResults.mGeoCodedImagesInLibrary)
        
    }

    
    //made this a Type (class method) for hackery purposes
    class func dumpIt(text :String){
        let shouldDump = true //change to true to dump to the log file
        
        if(shouldDump){
        let date = Date();
        let dateStr = AppDelegate.getFormattedDate(date: date)
        let contentToAppend = "\(dateStr) :"+text+"\n" //append the current datestamp on
        let filePath = NSHomeDirectory() + "/Documents/" + "diagnostic_log.txt"
        
        //Check if file exists
        if let fileHandle = FileHandle(forWritingAtPath: filePath) {
            //Append to file
            fileHandle.seekToEndOfFile()
            fileHandle.write(contentToAppend.data(using: String.Encoding.utf8)!)
            fileHandle.closeFile();
        }
        else {
            //Create new file
            do {
                try contentToAppend.write(toFile: filePath, atomically: true, encoding: String.Encoding.utf8)
            } catch {
                print("Error creating \(filePath)")
            }
        }
        }
    }
    
     class func getFormattedDate(date: Date) -> String{
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "dd-MM-yyyy'T' HH:mm:ss ZZZZZ"
        let formattedDate = formatter.string(from: date)
      //  print("DATE IS: \(formattedDate)")
        return formattedDate
    }
    
   
}

