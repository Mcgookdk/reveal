//
//  ServerLogger.swift
//  Reveal
//
// Base class to do the server logging
//  Created by David McGookin on 04/04/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import Foundation



class ServerLogger: NSObject{

    private var mHaveLogin = false;
    private var mLoginID: Int = -1;
    
  private   let INVALID_USERNAME = "INVALID_USERNAME"
    private var mUsername: String = "";
    

    
    
    private var mLoggingServerController =  LogingSeverController();
    
    private  var mLocalEventDBLogger = LocalEventDBLogger(); //Log the db events 
    
    override init(){
    
        //Check if we have the loginID already
   //  self.mServerSettings = PlistManager(plistName: pListName)
   //   self.mServerSettings.startPlistManager()
        super.init();
         self.mUsername = self.INVALID_USERNAME
         mLoggingServerController = LogingSeverController()
        
        let loginID = PlistManager.sharedInstance.getValueForKey(key: "UserID") as? Int
        // PlistManager.sharedInstance.saveValue(value: mLatestFireTimeHours, forKey: "LatestNotificationTimeHours")
        //}
        //   mServerSettings.getValueForKey(key: "UserID") as? Int
        if(loginID != mLoginID){ //i.e. invalid negative number
            mLoginID = loginID!;
           // mHaveLogin = false;
            
           self.mUsername = PlistManager.sharedInstance.getValueForKey(key: "ParticipantIdentifier") as! String
            let msgStr = ("SL: Have Login ID at init \(mLoginID) with Username \(self.mUsername)")
            ServerLogger.dumpIt(text: msgStr)
        //    print(msgStr)
            
            
            //attempting login
           self.login(username:self.mUsername)
          
        }else{
            let msgStr = ("SL: Do not have Login ID at init")
            ServerLogger.dumpIt(text: msgStr)
        //    print(msgStr)
        }
    }

    
    func register(username: String){
        let loginID = PlistManager.sharedInstance.getValueForKey(key: "UserID") as? Int
        // PlistManager.sharedInstance.saveValue(value: mLatestFireTimeHours, forKey: "LatestNotificationTimeHours")
        //}
        //   mServerSettings.getValueForKey(key: "UserID") as? Int
        if(loginID != mLoginID){ //i.e. invalid negative number
           //Then we have the login and have registered already, so we don't need to do this
            print("we are already registered")
            return;
        }
        
        
        if(!mHaveLogin){ //We have registered but have not quite yet actually logged in
            
         mUsername = username;
        print ("Setting Timer")
        let timer = Timer.scheduledTimer(timeInterval: 2, target:self, selector: Selector("delayReg"), userInfo: nil, repeats: false)
        }else{
           
            let msgStr = ("SL: We have login for server already, so we don't need to register again")
            ServerLogger.dumpIt(text: msgStr)
            print(msgStr)
        }
    }


    func delayReg(){
        print("Registering with Delay")
        
            print("don't yet have id so registering")
        mLoggingServerController.register(username: mUsername, password: mUsername, completion: { (value: Bool, str: String?) -> () in
            let msgStr = ("SL: Registration Status \(value), msg: \(str)")
            ServerLogger.dumpIt(text: msgStr)
            print(msgStr)
            
            if(value == true){
                //We are logged in
                self.mHaveLogin = value;
                self.mLoginID = Int(str!)!
              //  self.mServerSettings.saveValue(value: self.mLoginID as AnyObject, forKey: "UserID")
                PlistManager.sharedInstance.saveValue(value: self.mLoginID as NSNumber, forKey: "UserID")
                
                let loginID = PlistManager.sharedInstance.getValueForKey(key: "UserID") as? Int
                print("Have saved Login ID as \(loginID)")
            }
    });
        
    }
    
    
    func login(username: String){
        if(!mHaveLogin){
        mLoggingServerController.login(username: username, password: username, completion: { (value: Bool, str: String?) -> () in
            let msgStr = ("SL: Login Status \(value), msg: \(str)")
            ServerLogger.dumpIt(text: msgStr)
          //  print(msgStr)
            
            if(value == true){
                //We are logged in
                self.mHaveLogin = value;
                self.mLoginID = Int(str!)!
             
                PlistManager.sharedInstance.saveValue(value: self.mLoginID as NSNumber, forKey: "UserID")
              //  self.mServerSettings.saveValue(value: self.mLoginID as AnyObject, forKey: "UserID")
            }
        });
        }else{
            let msgStr = ("SL: We have login for server already, so we don't need to login again")
            ServerLogger.dumpIt(text: msgStr)
          //  print(msgStr)
        }
    }


    func logDevStat(formattedDate: String, dateSince1970: String, devID: String, osVersion: String, totalImages: Int, geoImages: Int){
    
        //Log to tthe local cahce db first
       let localID =  mLocalEventDBLogger.logLocalDevStat(formattedDate: formattedDate, dateSince1970: dateSince1970,  devID: devID, osVersion: osVersion, totalImages: totalImages, geoImages: geoImages)
      //  print("There are \(mLocalEventDBLogger.outstandingLocalDevStats()) local events in the DB")
        
 /*   This is commented out so we log only locally     if(mHaveLogin){
    mLoggingServerController.logDeviceStats(formattedDate: formattedDate, dateSince1970:  dateSince1970, devID: devID, osVersion: osVersion, totalImages: totalImages, geoImages: geoImages, completion: { (value: Bool, str: String?) -> () in
        if(value == true){// it inserted ok
            //then we can remove it from the local database
            self.mLocalEventDBLogger.removeLoggedDevStat(primaryKey: localID);
            
        }
        
        
        let msgStr = ("SL: Dev Stats Upload Status \(value), msg: \(str)")
        ServerLogger.dumpIt(text: msgStr)
        print(msgStr)
    });
            
            
   
            
        }else{
       //     print("No Login, No Logging")
        }
 */
}

    
    func logEvent(formattedDate: String, dateSince1970: String, eventCode: Int, description: String){
        
        //log to the local cache db first
        let localID =   mLocalEventDBLogger.logLocalEvent(formattedDate: formattedDate, dateSince1970: dateSince1970, eventCode: eventCode, description: description);
        //print("Existing Events Are\(mLocalEventDBLogger.r) local events in the DB")
        // mLocalEventDBLogger.retrieveLoggedLocalEvents(maxNumber: 10)
        
        
     /*  Commented out, we only log locally at the moment  if(mHaveLogin){
            mLoggingServerController.logEvent(formattedDate: formattedDate, dateSince1970:  dateSince1970, eventCode: eventCode, description: description, completion: { (value: Bool, str: String?) -> () in
                let msgStr = ("SL: Log Event Status \(value), msg: \(str)")
                if(value == true){// it inserted ok
                    //then we can remove it from the local database
                    self.mLocalEventDBLogger.removeLoggedLocalEvent(primaryKey: localID);
                    
                    
                }
                ServerLogger.dumpIt(text: msgStr)
                //  print(msgStr)
            });
            
        }else{
            print("No Login, No Logging")
        }
 */
    }
    
    /*Utility Function to determine if we are logged into the server or not*/
    func loggedIntoServer() -> Bool{
        return mHaveLogin;
    }
    
    func flushServerLogCache(){
        //We try to upload the cached entries that have not been previousl sucessfuly uploaded
        self.uploadCachedDevStatEntries()
        self.uploadCachedLogEntries()
        
    }

    private   func logCachedEvent(localID: Int, formattedDate: String, dateSince1970: String, eventCode: Int, description: String){
        
        
        if(mHaveLogin){
            mLoggingServerController.logEvent(formattedDate: formattedDate, dateSince1970:  dateSince1970, eventCode: eventCode, description: description, completion: { (value: Bool, str: String?) -> () in
              
                if(value == true){// it inserted ok
                    //then we can remove it from the local database
                    self.mLocalEventDBLogger.removeLoggedLocalEvent(primaryKey: localID);
                    
                    
                
                }
                  let msgStr = ("SL: Log Cached Event Status \(value), msg: \(str)");                ServerLogger.dumpIt(text: msgStr)
              //  print(msgStr)
            });
        }else{
          //  print("CACHED EVENT No Login, No Logging")
        }
    }

    
    private  func logCachedDevStat(localID: Int, formattedDate: String, dateSince1970: String, devID: String, osVersion: String, totalImages: Int, geoImages: Int){
        
        
        
        if(mHaveLogin){
            mLoggingServerController.logDeviceStats(formattedDate: formattedDate, dateSince1970:  dateSince1970, devID: devID, osVersion: osVersion, totalImages: totalImages, geoImages: geoImages, completion: { (value: Bool, str: String?) -> () in
                if(value == true){// it inserted ok
                    //then we can remove it from the local database
                    self.mLocalEventDBLogger.removeLoggedDevStat(primaryKey: localID);
                    
                }
                
                
                let msgStr = ("SL: Cached  Dev Stats Upload Status \(value), msg: \(str)")
                ServerLogger.dumpIt(text: msgStr)
              //  print(msgStr)
            });
        }else{
           // print("No Login, No Logging")
        }
    }
    
    
    
  private   func uploadCachedLogEntries(){
        
        //are there any
        if(mLocalEventDBLogger.outstandingLocalEvents() >= 1){
            
            let msgStr = ("SL: Have Outstanding Local Events. Attempting to Log them")
            ServerLogger.dumpIt(text: msgStr)
         //   print(msgStr)
            
            
        //get the first 20
            let currentResults = mLocalEventDBLogger.retrieveLoggedLocalEvents(maxNumber: LocalEventDBLogger.ALL_RESULTS); //Again, not throttling be careful
        
            for entry in currentResults{
            
                let formattedDate = entry[LocalEventDBLogger.FORMATTED_TIMESTAMP_STR]
                let timestampStr = entry[LocalEventDBLogger.TIMESTAMP_STR]
                let eventCode = entry[LocalEventDBLogger.EVENT_CODE_STR]
                let description = entry[LocalEventDBLogger.DESCRIPTION_STR]
                let localID = entry[LocalEventDBLogger.ID_STR]
                
                self.logCachedEvent(localID: localID as! Int, formattedDate: formattedDate as! String, dateSince1970: timestampStr as! String, eventCode: eventCode as! Int, description: description as! String)
            }
        }
    }

  private  func uploadCachedDevStatEntries(){
        
        //are there any
        if(mLocalEventDBLogger.outstandingLocalDevStats() >= 1){
            
            let msgStr = ("SL: Have Outstanding Dev Stats. Attempting to Log them")
            ServerLogger.dumpIt(text: msgStr)
          //  print(msgStr)
            
            
            //get the first 20
            let currentResults = mLocalEventDBLogger.retrieveLoggedLocalDeviceStats(maxNumber: LocalEventDBLogger.ALL_RESULTS); //We don't throttle this so be careful for issues
            
            for entry in currentResults{
                
                let formattedDate = entry[LocalEventDBLogger.FORMATTED_TIMESTAMP_STR]
                let timestampStr = entry[LocalEventDBLogger.TIMESTAMP_STR]
                let devID  = entry[LocalEventDBLogger.DEVID_STR]
                let osVersion = entry[LocalEventDBLogger.OSVERSION_STR]
                let totalImages = entry[LocalEventDBLogger.TOTAL_IMAGES_STR]
                let geoImages = entry[LocalEventDBLogger.GEO_IMAGES_STR]
                let localID = entry[LocalEventDBLogger.ID_STR]
                
                self.logCachedDevStat(localID: localID as! Int, formattedDate: formattedDate as! String, dateSince1970: timestampStr as! String, devID: devID as! String, osVersion: osVersion as! String, totalImages: totalImages as! Int, geoImages: geoImages as! Int)
               
            }
        }
    }
    
    
    
    //made this a Type (class method) for hackery purposes
    class func dumpIt(text :String){
        let date = Date();
        let contentToAppend = "\(date) :"+text+"\n" //append the current datestamp on
        let filePath = NSHomeDirectory() + "/Documents/" + "server_diagnostic_log.txt"
        
        //Check if file exists
        if let fileHandle = FileHandle(forWritingAtPath: filePath) {
            //Append to file
            fileHandle.seekToEndOfFile()
            fileHandle.write(contentToAppend.data(using: String.Encoding.utf8)!)
            fileHandle.closeFile();
        }
        else {
            //Create new file
            do {
                try contentToAppend.write(toFile: filePath, atomically: true, encoding: String.Encoding.utf8)
            } catch {
                print("Error creating \(filePath)")
            }
        }
    }


}
