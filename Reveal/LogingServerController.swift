//
//  LogingServerController.swift
//
//
//  Created by David McGookin on 29/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import Foundation

extension String {
    func sha1() -> String {
        let data = self.data(using: String.Encoding.utf8)!
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0, CC_LONG(data.count), &digest)
        }
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
}


class LogingSeverController: NSObject{
    
    let SERVER_IP_ADDRESS = "52.210.158.51" //"52.214.244.159" //replace this with the correct address
    
    //some public codes for the messages the server returns
    let LOGIN_REGISTRATION_SUCESSFUL = 700; //msg contains session id
    let REGISTRATION_FAILED_USER_EXISTS = 701;
    let LOGIN_FAILED = 702; //probab a usermane password error
    let REGISTRATION_FAILED = 703; //registration failed unknown error
    
    let INSERTION_SUCESS = 704; //we inserted into the db ok
    let INSERTION_FAILURE = 705; // the insertion into the db failed
    
    let GENERAL_ERROR = 799; //Used generally when the server checks if an insertion request is valid. Its deliberately obtuse incase of security
    
    
    
    
    private  let saltyMcSaltface = "adlfu3489tyh2jnkLIUGI&%EV(&0982ciwy4tgsf84587945789huweiukshjfkbakjhweoy8o85yvoiywatkjhfkjhseacoyweiOCWYOYTVbgrykxjnk8855" //we use the salt to hash the pass word with
    
    private    var mCurrentUserSessionId: Int = -1;
    private   var mCurrentUserId: String = "";
    private   var mCurrentUserPassword: String = "";
    
      
    
    
    func login(username: String, password: String, completion: @escaping (_ success: Bool, _ message: String?) -> ()) {
        
        mCurrentUserId = username
        mCurrentUserPassword = password
        
        let saltyPassword = self.hashedPassword(pass: password)
        logreg(email: username, password:saltyPassword, command: "login", completion: completion);
    }
    
    func register(username: String, password: String, completion: @escaping (_ success: Bool, _ message: String?) -> ()) {
        mCurrentUserId = username
        mCurrentUserPassword = password
        
        let saltyPassword = self.hashedPassword(pass: mCurrentUserPassword)
        logreg(email: mCurrentUserId, password:saltyPassword, command: "register", completion: completion);
    }
    
    //Logs an event out to the DB
    func logEvent(formattedDate: String, dateSince1970: String, eventCode: Int, description: String, completion: @escaping (_ success: Bool, _ message: String?) -> ()) {
        let payloadObject = ["command": "log_event", "username":mCurrentUserId, "date": formattedDate, "datems":dateSince1970, "sessionid": mCurrentUserSessionId, "event_code": eventCode, "description": description] as [String : Any]
        
        post(request: clientURLRequest(path: "", params: payloadObject as Dictionary<String, AnyObject>?)) { (success, object) -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                if success {
                   // print("sucess \(object)")
                    //Just because it was sucessful, doesn't mean it actually was!
                    let retCode = object?["retCode"] as? Int
                    
                    if(retCode != self.INSERTION_SUCESS){
                      //  print("ERROR ERRRO ERRO")
                        //error
                        completion(false, "Failed to Log Event \(retCode)")
                    }else{
                        //sucess
                        
                        completion(true, "Log Event Sucess");
                    }
                    
                } else {
                    var message = "there was an error"
                  //  print(message)
                  //  print(object)
                    completion(false, message)
                }
            })
        }
        
        
    }
    

        
        
        
        
        
    func logDeviceStats(formattedDate: String, dateSince1970: String, devID: String, osVersion:String, totalImages: Int, geoImages: Int, completion: @escaping (_ success: Bool, _ message: String?) -> ()) {
        
        let payloadObject = ["command": "log_device_stats", "username":mCurrentUserId, "date": formattedDate,"datems":dateSince1970, "sessionid": mCurrentUserSessionId, "deviceid": devID, "osversion":osVersion, "images": totalImages, "geoimages": geoImages] as [String : Any]
        
        post(request: clientURLRequest(path: "", params: payloadObject as Dictionary<String, AnyObject>?)) { (success, object) -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                if success {
                   // print("sucess \(object)")
                    //Just because it was sucessful, doesn't mean it actually was!
                    let retCode = object?["retCode"] as? Int
                    
                    if(retCode != self.INSERTION_SUCESS){
                     //   print("ERROR ERRRO ERRO")
                        //error
                        completion(false, "Failed to insert device stats \(retCode)")
                    }else{
                        //sucess
                        
                        completion(true, "Device Insertion Sucess");
                    }
                    
                } else {
                    var message = "there was an error"
                  //  print(message)
                 //   print(object)
                    completion(false, message)
                }
            })
        }

    
    }
    
    
    
    //hashes the plain text password into a salty snack
    private func hashedPassword(pass: String) -> String{
        var saltedPassword: String = ""
        saltedPassword = pass + saltyMcSaltface
       return  saltedPassword.sha1();
    }
    
    
    
    
    private func logreg (email: String, password: String, command: String,  completion: @escaping (_ success: Bool, _ message: String?) -> ()) {
        let loginObject = ["command": command, "username": email, "password": password]
        let urlRequest = clientURLRequest(path: "", params: loginObject as Dictionary<String, AnyObject>?)
      //  print(urlRequest)
        post(request:urlRequest ) { (success, object) -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                if success {
                   // print("sucess \(object)")
                    //Just because it was sucessful, doesn't mean it actually was!
                    let retCode = object?["retCode"] as? Int
                    
                    if(retCode != self.LOGIN_REGISTRATION_SUCESSFUL){
                      //  print("ERROR ERRRO ERRO")
                        //error
                        completion(false, "Registration Login Failed Code \(retCode)")
                    }else{
                        //sucess
                        
                        self.mCurrentUserSessionId = ((object!["msg"] as! NSString)).integerValue
                     //   print("Sucess \(self.mCurrentUserSessionId)");
                        completion(true, "\(self.mCurrentUserSessionId)");
                    }
                
                } else {
                    var message = "there was an error"
                  //  print(message)
                   // print(object)
                    completion(false, message)
                }
            })
        }
    }
    
    
   
    
    //Private functions for low level stuff on the server
    private func dataTask(request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        request.httpMethod = method
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
      //  print(request)
      //  print("Starting DATA TASK")
        session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
           // print("DATA TASK RETURN \(error)")
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode {
                    
                  //  print("OK")
                    print(json)
                    print(response)
                    completion(true, json as AnyObject?)
                } else {
                   // print("NOT OK: \(response)")
                    completion(false, json as AnyObject?)
                }
            }else{
               // print("ERROT:")
                completion(false, "ERROR" as AnyObject?)            }
            }.resume()
    }
    
    
    private func post(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request: request, method: "POST", completion: completion)
    }
    
    private func put(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request: request, method: "PUT", completion: completion)
    }
    
    private func get(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request: request, method: "GET", completion: completion)
    }
    

    private func clientURLRequest(path: String, params: Dictionary<String, AnyObject>? = nil) -> NSMutableURLRequest {
        let request = NSMutableURLRequest(url: NSURL(string: "http://\(SERVER_IP_ADDRESS)/"+path)! as URL)
        if let params = params {
            var paramString = ""
            for (key, value) in params {
                let escapedKey = key;//.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())
                let escapedValue = value;//.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())
                paramString += "\(escapedKey)=\(escapedValue)&"
         //       print(paramString)
            }
            
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = paramString.data(using: String.Encoding.utf8)
        }
        
        //  if let token = token {
        //     request.addValue("Bearer "+token, forHTTPHeaderField: "Authorization")
        //}
        
       // print(request.httpBody ?? "")
        return request
    }
    
    func completionHandler(result: Bool, code: String){
     //   print ("We FINISHED IN COMP HANDLER")
        
    }
}
