//
//  StatusViewController.swift
//  Reveal
//
// Debug View that shows status information about Reveal
//
//  Created by David McGookin on 02/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import UIKit

class StatusViewController: UIViewController {

    
    @IBOutlet weak var mLastStatusRefreshLabel: UILabel!
    @IBOutlet weak var mLastLocationUpdateLabel: UILabel!
    @IBOutlet weak var mLastPhotoSearchTimeAndLocationLabel: UILabel!
    @IBOutlet weak var mLastPhotoSearchStatsLabel: UILabel!
    @IBOutlet weak var mLastNotificationTriggerTimeLabel: UILabel!
    
    var mDelegate: StatusViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        //setup our tabbar item
        tabBarItem = UITabBarItem(title: "Status", image: #imageLiteral(resourceName: "status_tab_icon"), selectedImage: nil);
        navigationItem.title = "Reveal Status";
     
        
        //Need these to stop things disappearing behind the navigation bar and tab bar if they exist
        edgesForExtendedLayout = [];//stops the Top of the view going under the navigation bar
        extendedLayoutIncludesOpaqueBars = true;
        
        //add the refresh button to the toolbar
        let img = UIImage(named: "refresh_icon");
        let refreshBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(manuallyRefreshStatus))
        
        
        //  UIBarButtonItem(image:UIImage.init("refresh_icon"), target: self, action: #selector(manuallyRefreshItems))
        navigationItem.setRightBarButton(refreshBarButtonItem, animated: true)
        
    }
    
    func manuallyRefreshStatus(){
        refreshView()
    }
    
    func refreshView(){
        
        
        
        //createa date formmater to format all the dates
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.short
        formatter.timeStyle = DateFormatter.Style.long
    //    let str = formatter.string(from: (imageToDisplay?.creationDate)!)

        
        if((mDelegate) != nil){
            
            
            let nowTime = Date()
            let timeStr = formatter.string(from: (nowTime))
            mLastStatusRefreshLabel.text = "Last Update: \(timeStr)"
            
        let lastLocUpdate  = mDelegate?.lastLocationUpdate()
            if(lastLocUpdate != nil){
                  let str = formatter.string(from: (lastLocUpdate!.timestamp))
                mLastLocationUpdateLabel.text = "Last Location Update: Lat:\(lastLocUpdate!.coordinate.latitude) Lon:\(lastLocUpdate!.coordinate.longitude) at: \(str)"
            }
        let lastPhotoResults = mDelegate?.photoSearchResultsForLastLocation()
             let str = formatter.string(from: (lastPhotoResults!.resultDate()))
            mLastPhotoSearchTimeAndLocationLabel.text = "last Photo Results Date:\(str)"
            mLastPhotoSearchStatsLabel.text = "Total Images: \(lastPhotoResults!.mImagesInLibrary) Geo Images: \(lastPhotoResults!.mGeoCodedImagesInLibrary) In Area: \(lastPhotoResults!.mGeoCodedImagesInRequestedArea)"
            let str2 = formatter.string(from: (mDelegate!.lastNotificationTriggerTime()))
            mLastNotificationTriggerTimeLabel.text = "Last Notification \(str2)"
        }
    }
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
        refreshView()
    }
    func set(delegate: StatusViewControllerDelegate){
        mDelegate = delegate
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
