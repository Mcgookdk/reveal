//
//  PhotoInfoPopoverViewController.swift
//  Reveal
//
//  Created by David McGookin on 14/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import UIKit
import Photos
import MapKit

class PhotoInfoPopoverViewController:  UIViewController,  MKMapViewDelegate{

   
    @IBOutlet weak var mImageTakenOnLabel: UILabel!
   
    @IBOutlet weak var mImageTakenAtLabel: UILabel!
    
    
      var mImageAssetToDisplay: PHAsset!
    
    var mViewAppearDate: Date = Date()
    
    @IBOutlet weak var mMapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mMapView.delegate = self;
        
        navigationItem.title = "Image Details";
        //Need these to stop things disappearing behind the navigation bar and tab bar if they exist
        edgesForExtendedLayout = [];//stops the Top of the view going under the navigation bar
        extendedLayoutIncludesOpaqueBars = true;
        //add the refresh button to the toolbar
        let img = UIImage(named: "close_icon");
        let dismissBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(dismissView))
        
        navigationItem.setRightBarButtonItems([dismissBarButtonItem], animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_VIEWED_IMAGE_MAP_DETAILS, description: "User tapped info view for image")
        mViewAppearDate = Date()
        
        //update the date taken label
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.long
        formatter.timeStyle = DateFormatter.Style.none
        mImageTakenOnLabel.text = formatter.string(from: (mImageAssetToDisplay?.creationDate)!)
        
        formatter.dateStyle = DateFormatter.Style.none
        formatter.timeStyle = DateFormatter.Style.short
        mImageTakenAtLabel.text = formatter.string(from: (mImageAssetToDisplay?.creationDate)!)
        //set the map up
        let regionRadius: CLLocationDistance = 50
        let coordinateRegion = MKCoordinateRegionMakeWithDistance((mImageAssetToDisplay.location?.coordinate)!,regionRadius * 2.0, regionRadius * 2.0)
           mMapView.setRegion(coordinateRegion, animated: false)
        
            let mapAnnotation = MKPointAnnotation()
            mapAnnotation.coordinate = (mImageAssetToDisplay.location?.coordinate)!
            mMapView.addAnnotation(mapAnnotation)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        var viewOnScreenTimeSeconds:Double = 0.0
        viewOnScreenTimeSeconds = Date().timeIntervalSince(mViewAppearDate)
        EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_DISMISSED_IMAGE_MAP_DETAILS, description: "User dismissed info view, \(viewOnScreenTimeSeconds)")
    
    }
    func setAsset(asset: PHAsset){
        
        mImageAssetToDisplay = asset
        
    }

    
    func dismissView(){
        self.dismiss(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    //so we can set the image icon on the map!
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
  
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        
        let annotationIdentifier = "AnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = true
        }
        else {
            annotationView!.annotation = annotation
        }
        
        let pinImage = UIImage(named: "map_marker_icon")
        annotationView!.image = pinImage
        
        return annotationView
    }
}
