//
//  LargeScrollableImageViewController.swift
//  Reveal
//
//  VZoomable View Controller for big versions of the images
//
//  Created by David McGookin on 01/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import UIKit
import Photos

class LargeScrollableImageViewController: UIViewController, PHPhotoLibraryChangeObserver {

    @IBOutlet weak var mImageScrollView: ImageScrollView!
  
    @IBOutlet weak var mLoadingProgressLabel: UILabel!
    @IBOutlet weak var mLoadingProgressView: UIProgressView!
    @IBOutlet weak var mLoadingView: UIView!
  
    var mViewAppearDate: Date = Date()
    var mImageLoadingViewIsVisible = false;
    var mFavoriteBarButtonItem: UIBarButtonItem!
    var mImage: UIImage!
    var mImageAssetToDisplay: PHAsset!
    
    var mCurrentImageLoadingAssetTag: PHImageRequestID = 0;
       override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = [.bottom];//stops the Top of the view going under the navigation bar  
        PHPhotoLibrary.shared().register(self)
        
        
        
        self.mLoadingView.isHidden = true
        self.mLoadingView.backgroundColor = UILookAndFeel.defaultTitleTextColour()
        
      //  self.mLoadingProgressView.trackTintColor = UILookAndFeel.defaultTitleTextColour()
        self.mLoadingProgressView.tintColor = UILookAndFeel.defaultTintColour()
        self.mLoadingProgressLabel.textColor = UILookAndFeel.defaultTintColour()
        
        
        //add the refresh button to the toolbar
        let img = UIImage(named: "info_icon");
        let refreshBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(presentImageDetails))

        loadAsset()
        
        var img2: UIImage!;
        if(mImageAssetToDisplay.isFavorite){
            img2 = UIImage(named: "favorite_image_icon")
        }else{
            img2 = UIImage(named: "not_favorite_image_icon")
        }
        mFavoriteBarButtonItem = UIBarButtonItem(image: img2, style: UIBarButtonItemStyle.plain, target: self, action: #selector(toggleAsFavoriteImage))
        
        navigationItem.setRightBarButtonItems([refreshBarButtonItem, mFavoriteBarButtonItem], animated: true)
    }
    
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //update the date taken label
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.short
        formatter.timeStyle = DateFormatter.Style.none
        self.navigationItem.title = formatter.string(from: (mImageAssetToDisplay?.creationDate)!)
        EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_OPENED_SCROLLABLE_IMAGEVIEW, description: "Scrollable Images for image id,  \(PhotoManager.getStringID(img: mImageAssetToDisplay!)),  with date \(mImageAssetToDisplay!.creationDate) was opened")
        mViewAppearDate = Date()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        var viewOnScreenTimeSeconds:Double = 0.0
        viewOnScreenTimeSeconds = Date().timeIntervalSince(mViewAppearDate)
        
        EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_DISMISSED_SCROLLABLE_IMAGEVIEW, description: "Scrollable Images for image id,  \(PhotoManager.getStringID(img: mImageAssetToDisplay!)), with date \(mImageAssetToDisplay!.creationDate) was dismissed after (seconds), \(viewOnScreenTimeSeconds)")
        
        PHImageManager().cancelImageRequest(mCurrentImageLoadingAssetTag) //Cancel a loading image if we have one
        
    }
    
    func toggleAsFavoriteImage(){
        var img: UIImage!
        if(mImageAssetToDisplay?.isFavorite)!{
            img = UIImage(named:"not_favorite_image_icon")
            EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_UNFAVORITED_PHOTO, description: "User un-favorited image (in scrollable view)  with creation date \(mImageAssetToDisplay!.creationDate) and id, \(PhotoManager.getStringID(img: mImageAssetToDisplay!)) ")
            PhotoManager.setFavoriteStatus(asset:mImageAssetToDisplay!, status: false)
            
        }else{
            img = UIImage(named:"favorite_image_icon")
            EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_FAVORITED_PHOTO, description: "User favorited image (in scrollable view) with creation date \(mImageAssetToDisplay!.creationDate) and id, \(PhotoManager.getStringID(img: mImageAssetToDisplay!))")
            PhotoManager.setFavoriteStatus(asset: mImageAssetToDisplay!, status: true)
        }
        
        
        mFavoriteBarButtonItem.image = img;
 
    }
    
    
    
    func presentImageDetails(){
        
        let navController = UINavigationController();
        navController.view.backgroundColor = UIColor.clear;
         navController.navigationBar.barTintColor =  UILookAndFeel.defaultTintColour()
        navController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UILookAndFeel.defaultTitleTextColour()]
        navController.navigationBar.tintColor = UILookAndFeel.defaultTitleTextColour()
        //navController.navigationBar.isHidden = true
        
        let photoDetailsViewController = PhotoInfoPopoverViewController()
        photoDetailsViewController.setAsset(asset: mImageAssetToDisplay)
        //prefManagerViewController.modalPresentationStyle = .overFullScreen;

        navController.modalPresentationStyle = .pageSheet
        navController.modalTransitionStyle = .flipHorizontal;
        navController.pushViewController(photoDetailsViewController, animated: false);
        
        present(navController, animated: true, completion: nil)
        print("presented preference controller");
    }
    
    func setAsset(asset: PHAsset){
        
        mImageAssetToDisplay = asset
        
    }
    
    func loadAsset(){
     //   let scale = UIScreen.main.scale;
       // let cellSize = mImageAssetToDisplay.bounds // mImageView.bounds;//   CGSize(width: 500.0, height: 500.0);
       // let thumbnailSize = CGSize(width: CGFloat(mImageAssetToDisplay.pixelWidth) * scale, height: CGFloat(mImageAssetToDisplay.pixelHeight) * scale)
         let thumbnailSize = CGSize(width:mImageAssetToDisplay.pixelWidth, height: mImageAssetToDisplay.pixelHeight)
        print("BIG IMAGE Media Type is : \(mImageAssetToDisplay.mediaType) with Size \(thumbnailSize)");
        let requestOptions = PHImageRequestOptions();
        requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.opportunistic
        requestOptions.isSynchronous = false
        requestOptions.version = PHImageRequestOptionsVersion.current
        requestOptions.isNetworkAccessAllowed = true; //need this or the image requestight fail. AS iCloud can't get to network //we took it out and it didn't work, so we really need this
        requestOptions.progressHandler = { progress, _, _, _ in
            // Handler might not be called on the main queue, so re-dispatch for UI work.
            DispatchQueue.main.sync {
                if(progress < 1.0){ //progress is from 0 to 1
                    self.mLoadingView.isHidden = false
                    self.mLoadingProgressView.setProgress(Float(progress), animated: true)
                }else{
                    self.mLoadingView.isHidden = true;
                }
                print("BIG IMAGE Image Loading Progress \(progress)")
            }
        }
        mCurrentImageLoadingAssetTag = PHImageManager().requestImage(for: mImageAssetToDisplay, targetSize: thumbnailSize, contentMode: .aspectFit, options: requestOptions, resultHandler: {(image, info)->Void in
            
            
            
            
            print ("BIG IMAGE INFO HAS DATA \(info)");
            
            let error = info![PHImageErrorKey]; //info has to exist here since we check for it
            if(error == nil){
                print("BIG IMAGE NO ERROR")
            }else{
                print("BIG IMAGE THERE WAS AN ERROR: \(error)");
                
            }
            
            
            //}else{
            //  print("BIG IMAGE INFO HAS DATA \(info)");
            
            //}
            //   let error = info?[PHImageErrorKey];
            // if(error == nil){
            //   print("NO ERROR")
            //}else{
            //  print("THERE WAS AN ERROR: \(error)");
            
            //}
            
            
            if(image != nil){// else { print("IMAGE IS CRAP BIG IMG"); return };
                
                print("Have Big Image Size \(image?.size)")
                // print("Image was set in big scrolling view  image view");
                //print("WE GOT THE IMAGE AND ARE SETTING IN BIG SCROLL VIEW")
                self.mImage = image
                //  let img = UIImage(named: "dog-2.jpg")
                self.mImageScrollView.display(image: self.mImage!)
                // self.mImageView.image = image;
                //  self.mImageView.isUserInteractionEnabled = true;
                
                //       
            }else{
                print("LARGE IMAGE IMAGE IS CRAP!");
                self.loadAsset() //This is a bit of a hack to retry loading the image again! If it fails. Sometimes works sometimes doesn't  Probabl should be a guard here somewhere
            }
            
        }
            
            
        )
        
        
        
        
        
        
        //load the image, then set it as the display image
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

        func photoLibraryDidChange(_ changeInstance: PHChange) {
            print("PHOTO CHANGED")
            // Call might come on any background queue. Re-dispatch to the main queue to handle it.
            DispatchQueue.main.sync {
                // Check if there are changes to the asset we're displaying.
                guard let details = changeInstance.changeDetails(for: mImageAssetToDisplay) else { return }
                
                // Get the updated asset.
                mImageAssetToDisplay = details.objectAfterChanges as! PHAsset
                
                // If the asset's content changed, update the image and stop any video playback.
                if details.assetContentChanged {
                    print("CONTENT CHANGED")
                   // updateImage()
                  ///
                  //  playerLayer?.removeFromSuperlayer()
                 //   playerLayer = nil
                }
            }
        }
    }
