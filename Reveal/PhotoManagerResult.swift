//
//  PhotoManagerResult.swift
//  Reveal
//
//
//  A Result class to cover results from the PhotoManager
//  Prob super hacky but its the way we are doing it
//
//  Created by David McGookin on 01/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import Foundation
import Photos


class PhotoManagerResult : NSObject,  PHPhotoLibraryChangeObserver{
    
   static let INVALID_PHOTO_RESULT_VALUE = -1000
    
    var mImagesInLibrary = INVALID_PHOTO_RESULT_VALUE
    var mGeoCodedImagesInLibrary = INVALID_PHOTO_RESULT_VALUE
    var mGeoCodedImagesInRequestedArea = INVALID_PHOTO_RESULT_VALUE
    var mNumUnseenGeoImagesInRequestedArea = INVALID_PHOTO_RESULT_VALUE
    var mGeoImagesInRequestedArea: [PHAsset]? = nil
    var mUnseenGeoImagesInRequestedArea: [PHAsset]? = nil
    var mResultsObtainedDate: Date = Date()
    var mEarliestGeoImageCreationDate: Date = Date()
    var mEarliestGeoImageModificationDate: Date = Date()
    
    
    func  strDescription() -> String{
        return "PhotoMangerResult: obtainedAt:\(mResultsObtainedDate) Images:\(mImagesInLibrary) GeoImages:\(mGeoCodedImagesInLibrary) GeoImagesInArea:\(mGeoCodedImagesInRequestedArea) OldestGeoImage (creation):\(mEarliestGeoImageCreationDate) OldestGeoImage (modification):\(mEarliestGeoImageModificationDate)"
    }
    
    func  csvStrDescription() -> String{
        return "PhotoMangerResult, obtainedAt,\(mResultsObtainedDate), Images,\(mImagesInLibrary), GeoImages,\(mGeoCodedImagesInLibrary), GeoImagesInArea,\(mGeoCodedImagesInRequestedArea),OldestGeoImage (creation),\(mEarliestGeoImageCreationDate),OldestGeoImage (modification),\(mEarliestGeoImageModificationDate),"
    }
    init(obtainedAt: Date, imagesInLib: Int, geoImagesInLib: Int, numGeoImagesInRequestedArea: Int, numUnseenGeoImagesInRequestedArea: Int,  geoImageAssetsInRequestedArea: [PHAsset], unseenGeoImagesInRequestedArea: [PHAsset],
        earliestGeoImageCreationDate: Date,
        earliestGeoImageModificationDate: Date){
            super.init()
        mResultsObtainedDate = obtainedAt
        mImagesInLibrary = imagesInLib
        mGeoCodedImagesInLibrary = geoImagesInLib
        mGeoCodedImagesInRequestedArea = numGeoImagesInRequestedArea
        mGeoImagesInRequestedArea = geoImageAssetsInRequestedArea
        mUnseenGeoImagesInRequestedArea = unseenGeoImagesInRequestedArea
        mNumUnseenGeoImagesInRequestedArea = numUnseenGeoImagesInRequestedArea
        mEarliestGeoImageCreationDate = earliestGeoImageCreationDate
        mEarliestGeoImageModificationDate = earliestGeoImageModificationDate
        
        
        PHPhotoLibrary.shared().register(self)
        
    }

    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }

    func resultDate() -> Date{
        return mResultsObtainedDate
    }
    
    func numberOfImagesInLibrary() -> Int{
        return mImagesInLibrary
    }
    
    func numberOfGeoCodedImagesInLibrary() -> Int{
        return mGeoCodedImagesInLibrary
    }


    func numberOfGeoCodedImagesInRequestedArea() -> Int{
        return mGeoCodedImagesInRequestedArea
    }
    
    func geoImagesInRequestedArea() -> [PHAsset]{
        return mGeoImagesInRequestedArea!
    }
    
    func numberOfUnseenGeoCodedImagesInRequestedArea() -> Int{
        return mNumUnseenGeoImagesInRequestedArea
    }
    
    func unseenGeoImagesInRequestedArea() -> [PHAsset]{
        return mUnseenGeoImagesInRequestedArea!
    }
    
    func earliestGeoImageCreationDate() -> Date{
        return mEarliestGeoImageCreationDate
    }
    func earliestGeoImageModificationDate() -> Date{
        return mEarliestGeoImageModificationDate
    }
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        print("PHOTO CHANGED IN PHOTO LIB")
        // Call might come on any background queue. Re-dispatch to the main queue to handle it.
        DispatchQueue.main.sync {
            //we update the current results to reflect any changes.
            if (mGeoCodedImagesInRequestedArea < 2){
                EventLogger.sharedInstance.LogEvent(code: -100, description: "BOUNDS ERROR CHECK RETURNED FOR NUM IMAGES")
                return
            }
            
            for i in 0...mGeoCodedImagesInRequestedArea-1{
                let details = changeInstance.changeDetails(for: (mGeoImagesInRequestedArea?[i])!)
                if((details) != nil){
                    mGeoImagesInRequestedArea?[i] = details?.objectAfterChanges as! PHAsset
                }
            }
            
            if(mNumUnseenGeoImagesInRequestedArea < 2){
                EventLogger.sharedInstance.LogEvent(code: -100, description: "BOUNDS ERROR CHECK RETURNED FOR UNSEEN IMAGES")
                return
            }
            
            for i in 0...mNumUnseenGeoImagesInRequestedArea-1{
                let details = changeInstance.changeDetails(for: (mUnseenGeoImagesInRequestedArea?[i])!)
                if((details) != nil){
                    mUnseenGeoImagesInRequestedArea?[i] = details?.objectAfterChanges as! PHAsset
                }
            }
        }
    }
}


