//
//  EncapsulatedLocationManagerDelegate.swift
//  Reveal
//
//   Delegate for the Encapsulated Location Manager. Only adds one method to communicate if user  authorsiation failed
//
//  Created by David McGookin on 28/02/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import Foundation
import CoreLocation


protocol EncapsulatedLocationManagerDelegate: class{
    
    //CLLocationManagerDelegates
// func encapsulatedLocationManagerDidPauseLocationUpdates(_ manager: CLLocationManager);
    
 //func encapsulatedLocationManagerDidResumeLocationUpdates(_ manager: CLLocationManager);
    
func encapsulatedLocationManagerDidBackgroundLocationUpdates(_ manager: CLLocationManager);
    
func encapsulatedLocationManagerDidForegroundLocationUpdates(_ manager: CLLocationManager);
    
    
 func encapsulatedLocationManagerDidUpdateLocations (locations: [CLLocation]);
    
// func encapsulatedLocationManager(_ manager: CLLocationManager, didFailWithError error: Error);
   
 func encapsulatedLocationMangerAuthorisationDidFail(status: CLAuthorizationStatus);
    
    //called when the user leaves a region set by the system. 
    func encapsulatedLocationManagerUserDidExitRegion(id: String);
    
}
