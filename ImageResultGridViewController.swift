//
//  ImageResultGridViewController.swift
//  Reveal
//
//  Created by David McGookin on 01/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import UIKit
import Photos
import PhotosUI

private let reuseIdentifier = "MyCell"
private let headerReuseIdentifier = "YearSectionHeader"
var mViewAppearDate: Date = Date()

private extension UICollectionView {
    func indexPathsForElements(in rect: CGRect) -> [IndexPath] {
        let allLayoutAttributes = collectionViewLayout.layoutAttributesForElements(in: rect)!
        return allLayoutAttributes.map { $0.indexPath }
    }
}


class ImageResultGridViewController: UICollectionViewController {
    
    struct GridSectionInfo{
        var startIndex = 0
        var endIndex = 0
        var sectionTitle = "XXXX"
    }
    
    var mImagesToGridMappingArray = [GridSectionInfo]()
    
    var mCurrentPhotoResult: PhotoManagerResult? = nil //holds teh current results from the last search query
    var mThumbnailSize: CGSize! //The size of the thumbnail images we request
    let imageManager = PHCachingImageManager()
    var previousPreheatRect = CGRect.zero
    
    
    convenience init(){
        //This class needs to be initialised with a layout 
        let layoutParams = UICollectionViewFlowLayout();
        layoutParams.minimumLineSpacing = 0.0
        layoutParams.minimumInteritemSpacing = 0.0
        layoutParams.headerReferenceSize = CGSize.init(width: 0, height: 30) //*** header ref size
        //we want 3 across the screen
        let imageWidth = UIScreen.main.bounds.size.width / 3.0
        layoutParams.itemSize = CGSize(width: imageWidth , height: imageWidth)
        self.init(collectionViewLayout: layoutParams)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        navigationItem.title = "Other Nearby Images";
        self.collectionView?.backgroundColor = UIColor.white
        
        //Need these to stop things disappearing behind the navigation bar and tab bar if they exist
       // edgesForExtendedLayout = [];//stops the Top of the view going under the navigation bar
      //  extendedLayoutIncludesOpaqueBars = true;
        
        
        collectionView!.register(UINib(nibName: "ImageResultGridViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
    collectionView!.register(UINib(nibName: "ImageGridHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerReuseIdentifier)
        resetCachedAssets()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Determine the size of the thumbnails to request from the PHCachingImageManager
        let scale = UIScreen.main.scale
        let cellSize = (collectionViewLayout as! UICollectionViewFlowLayout).itemSize
        mThumbnailSize = CGSize(width: cellSize.width * scale, height: cellSize.height * scale)
        EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_OPENED_GRID_VIEW, description: "User opened grid view with \(mCurrentPhotoResult?.mGeoCodedImagesInRequestedArea) images")
          mViewAppearDate = Date()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        var viewOnScreenTimeSeconds:Double = 0.0
        viewOnScreenTimeSeconds = Date().timeIntervalSince(mViewAppearDate)
        EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_DISMISSED_GRID_VIEW, description: "User dismissed grid view with \(mCurrentPhotoResult?.mGeoCodedImagesInRequestedArea) images after (seconds), \(viewOnScreenTimeSeconds)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        updateCachedAssets()
        self.collectionView?.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func setImageResults(results: PhotoManagerResult){
        mCurrentPhotoResult = results;
        
        mImagesToGridMappingArray = [GridSectionInfo]() //Null the mapping array
        setupImageGridSectionMapping()
        //invalidate the grids and relayout
        self.collectionView?.reloadData()
        
    }
    
    
    func setupImageGridSectionMapping(){
        
        //update the date taken label
        let formatter = DateFormatter()
       formatter.locale = Locale(identifier: "en_GB")
        formatter.setLocalizedDateFormatFromTemplate("yyyy") // // set template after setting locale
        
        
        
        //we aassume the is at least one result, so we start with that
        var i = 0
        
        var currentSection = GridSectionInfo()
        let initialImage = mCurrentPhotoResult?.geoImagesInRequestedArea()[0]
        
        currentSection.startIndex = 0
        currentSection.sectionTitle = formatter.string(from: (initialImage?.creationDate)!)
        var maxInt = 9 //stupid swift and its maybe there is and isnt a value
        maxInt = ((mCurrentPhotoResult?.numberOfGeoCodedImagesInRequestedArea())!)-1
        
        for index in 1...maxInt{
            
            let currentImage =  mCurrentPhotoResult?.geoImagesInRequestedArea()[index]
            
            let yearOfCurrentImage = formatter.string(from: (currentImage?.creationDate)!)
            if(yearOfCurrentImage != currentSection.sectionTitle){
                //we have a new year so need to do some things
                currentSection.endIndex = index-1 //the last viewed image will be the end of the previous section
                mImagesToGridMappingArray.append(currentSection)
                currentSection = GridSectionInfo()
                currentSection.startIndex = index
                currentSection.sectionTitle = yearOfCurrentImage
            }
        }
        
        
        //at the end we need to fix up the last current section and add it to the array
        currentSection.endIndex =  maxInt //- 1 //the last but one element
        mImagesToGridMappingArray.append(currentSection)
        
        
            
        
    }
    
    func indexForIndexPath(ip: IndexPath) -> Int{
        let section = ip.section
        let item = ip.item
    
        print ("requesting \(section) \(item)  value")
    
        
        return (mImagesToGridMappingArray[section].startIndex + item)
    
        
    }
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateCachedAssets()
        
    }
       // MARK: UICollectionViewDataSource

    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return mImagesToGridMappingArray.endIndex
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
    
        // #warning Incomplete implementation, return the number of items
    if(mCurrentPhotoResult != nil){ //possible this might be nil so we reset
    let currentSection = mImagesToGridMappingArray[section]
    print ("No. Items is \(currentSection.endIndex - currentSection.startIndex)")
    return (currentSection.endIndex - currentSection.startIndex) + 1 //need to add 1 on so that we don't end up loosing images
        } else{
            return 0
        }
    
}
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> ImageResultGridViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ImageResultGridViewCell
        
       
    let asset = mCurrentPhotoResult?.geoImagesInRequestedArea()[indexForIndexPath(ip: indexPath)]
    
        cell.setFavorite(fav: (asset?.isFavorite)!) //show the love if its a fave

    
    // Request an image for the asset from the PHCachingImageManager.
    cell.representedAssetIdentifier = asset?.localIdentifier
    let requestOptions = PHImageRequestOptions();
    requestOptions.isNetworkAccessAllowed = true; //need this or the image request might fail. AS iCloud can't get to network
        requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.opportunistic//highQualityFormat
        requestOptions.version = PHImageRequestOptionsVersion.current
        
        
        
        
    imageManager.requestImage(for: asset!, targetSize: mThumbnailSize, contentMode: .aspectFill, options: requestOptions, resultHandler: { image, _ in
    // The cell may have been recycled by the time this handler gets called;
    // set the cell's thumbnail image only if it's still showing the same asset.
    if cell.representedAssetIdentifier == asset?.localIdentifier {
    cell.thumbnailImage = image
    }
    })
    
    return cell
    
}




    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
    
    override func collectionView(_ collectionView: UICollectionView,
                                 viewForSupplementaryElementOfKind kind: String,
                                 at indexPath: IndexPath) -> UICollectionReusableView {
        print ("RETURNING HEADER VIEW")
        
        if(kind == UICollectionElementKindSectionHeader){
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerReuseIdentifier,
                for: indexPath) as! ImageGridHeaderCollectionReusableView
        
         
            headerView.mYearLabel.text = mImagesToGridMappingArray[indexPath.section].sectionTitle
               return headerView
        }
        return UICollectionReusableView()
    }
    
    
    
     override func collectionView(_ collectionView: UICollectionView,
                                 didSelectItemAt indexPath: IndexPath){
        
        print ("USER TAPPED CELL")
         let asset = mCurrentPhotoResult?.geoImagesInRequestedArea()[indexForIndexPath(ip: indexPath)]
        let scrollableImageViewController = LargeScrollableImageViewController();//(collectionViewLayout: layoutParams)
        scrollableImageViewController.setAsset(asset: asset!)
        
        EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_TAPPED_IMAGE_IN_GRID_VIEW_CONTROLLER_FOR_LARGER_VIEW, description: "User requested large image with date \(String(describing: asset!.creationDate)) and ID \(PhotoManager.getStringID(img: asset!))")
        
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem //
        
        self.navigationController?.pushViewController(scrollableImageViewController, animated: true)
        
        
    }
    // MARK: Asset Caching
    
    fileprivate func resetCachedAssets() {
        imageManager.stopCachingImagesForAllAssets()
        previousPreheatRect = .zero
    }
    
    fileprivate func updateCachedAssets() {
        // Update only if the view is visible.
        guard isViewLoaded && view.window != nil else { return }
        
        // The preheat window is twice the height of the visible rect.
        let preheatRect = view!.bounds.insetBy(dx: 0, dy: -0.5 * view!.bounds.height)
        
        // Update only if the visible area is significantly different from the last preheated area.
        let delta = abs(preheatRect.midY - previousPreheatRect.midY)
        guard delta > view.bounds.height / 3 else { return }
        
        // Compute the assets to start caching and to stop caching.
        let (addedRects, removedRects) = differencesBetweenRects(previousPreheatRect, preheatRect)
        let addedAssets = addedRects
            .flatMap { rect in collectionView!.indexPathsForElements(in: rect) }
            .map { indexPath in mCurrentPhotoResult?.geoImagesInRequestedArea()[indexPath.item]};//fetchResult.object(at: indexPath.item) }
        let removedAssets = removedRects
            .flatMap { rect in collectionView!.indexPathsForElements(in: rect) }
            .map { indexPath in mCurrentPhotoResult?.geoImagesInRequestedArea()[indexPath.item]}; //{ indexPath in fetchResult.object(at: indexPath.item) }
        
        // Update the assets the PHCachingImageManager is caching.
        let requestOptions = PHImageRequestOptions();
        requestOptions.isNetworkAccessAllowed = true; //need this or the image request might fail. AS iCloud can't get to network
        imageManager.startCachingImages(for: addedAssets as! [PHAsset],
                                        targetSize: mThumbnailSize, contentMode: .aspectFill, options: requestOptions)
        imageManager.stopCachingImages(for: removedAssets as! [PHAsset],
                                       targetSize: mThumbnailSize, contentMode: .aspectFill, options: requestOptions)
        
        // Store the preheat rect to compare against in the future.
        previousPreheatRect = preheatRect
    }
    
    fileprivate func differencesBetweenRects(_ old: CGRect, _ new: CGRect) -> (added: [CGRect], removed: [CGRect]) {
        if old.intersects(new) {
            var added = [CGRect]()
            if new.maxY > old.maxY {
                added += [CGRect(x: new.origin.x, y: old.maxY,
                                 width: new.width, height: new.maxY - old.maxY)]
            }
            if old.minY > new.minY {
                added += [CGRect(x: new.origin.x, y: new.minY,
                                 width: new.width, height: old.minY - new.minY)]
            }
            var removed = [CGRect]()
            if new.maxY < old.maxY {
                removed += [CGRect(x: new.origin.x, y: new.maxY,
                                   width: new.width, height: old.maxY - new.maxY)]
            }
            if old.minY < new.minY {
                removed += [CGRect(x: new.origin.x, y: old.minY,
                                   width: new.width, height: new.minY - old.minY)]
            }
            return (added, removed)
        } else {
            return ([new], [old])
        }
    }
    
}



