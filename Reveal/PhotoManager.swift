//
//  PhotoManager.swift
//  Reveal
//
// handles all access to the photos linrary etc.
//
//
//  Created by David McGookin on 28/02/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import Foundation
import CoreLocation
import Photos


class PhotoManager {
    
  //  var cachedResult: PhotoManagerResult? = nil;
    
    
    
  //  //searches the library and returns the number of images and geo-images in it as a String Int Dictionary
   // func getLibraryMetaData() -> [String: Int]{
    //   var metaData = [String: Int]()
    //metaData["photos"]=100
     //   metaData["geophotos"] = 5
     // return metaData
   // }
    
    
    //returns (we hope) a good approx a unique id for this image in the photos library
    
    class func getStringID(img: PHAsset) -> String{
        var  creationDateTime =  NSTimeIntervalSince1970
        creationDateTime = (img.creationDate?.timeIntervalSince1970)!
        let checkCode = "\(creationDateTime)"
      //  print ("Code is \(checkCode)")
        return checkCode
        
    }
    
    class func resetSeenImages(){
        let db = SQLiteDB.shared
        let result = db.execute(sql:"DELETE FROM SeenImages")
        if (result == 0){
            print ("Resetting DB Failed!!!!")
        }
    }
    
    class func setImageAsPreviouslySeen(img: PHAsset){
        let strCode = getStringID(img: img)
        
        let db = SQLiteDB.shared
        let result = db.execute(sql:"INSERT INTO SeenImages VALUES ('\(strCode)')")
        if (result == 0){
            print ("insertion operation failed!!!!")
        }
    }
    
    
    class func imageFingerPrint(asset: PHAsset) -> String{
        return "\(asset.creationDate)"
    }
    
    //Given the PHAsset set its favorite status
    class func setFavoriteStatus(asset: PHAsset, status: Bool){
            PHPhotoLibrary.shared().performChanges({
                let request = PHAssetChangeRequest(for: asset)
                request.isFavorite = status;
            }, completionHandler: {success, error in
                if success {
                   print("SUCESS")
                } else {
                    print("can't set favorite: \(error)")
                }
            })
    }
    
    //returns the number of images in the given location and radius
  class func  searchImagesForLocation(location: CLLocation, radius: Double) ->  PhotoManagerResult{
    
    
    //setup the db
    
    let db = SQLiteDB.shared
    //let dbResult = db.query(sql:"SELECT * FROM SeenImages")
    
   // if(dbResult.count == 0){
   //     print("No data in the Sql DB")
  //  }else{
  //      print("DATA IN THE SQL DB")
  //  }
    
    
    var allPhotos: PHFetchResult<PHAsset>!
    var nearbyImages: [PHAsset]!
    var unseenNearbyImages: [PHAsset]!
    
    
    // Create a PHFetchResult object for each section in the table view.
    let allPhotosOptions = PHFetchOptions()
    allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
    allPhotos = PHAsset.fetchAssets(with: allPhotosOptions)
    
  //  smartAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .albumRegular, options: nil)
   // userCollections = PHCollectionList.fetchTopLevelUserCollections(with: nil)
    //allPhotos should represent all of the photographs in the photos library
    
    print("Found "+String(allPhotos.count)+" photographs. Searching Location \(location)");
    var numberOfGeocodedImages: Int = 0;
    var totalNumberOfImages: Int = 0;
    var geoImagesInCurrentArea: Int = 0;
    var unseenGeoImagesInCurrentArea: Int = 0;
    var geoCodedImagesIndexSet = IndexSet();
    var unseenGeoCodedImagesIndexSet = IndexSet();
    var earliestGeoImageCreationDate = Date();// We default to current date
    var earliestGeoImageModificationDate = Date();// We default to current date
    //  geoCodedImagesIndexSet.init();
    
    
    allPhotos.enumerateObjects(using: { (object: AnyObject,
        count: Int,
        stop: UnsafeMutablePointer<ObjCBool>)in
        
        
        if object is PHAsset{
            let currentImage = object as! PHAsset;
           //  print(currentImage);
            totalNumberOfImages+=1;
            if(currentImage.location != nil){
                numberOfGeocodedImages+=1;
                
                //check for oldest geoimage
                if(currentImage.creationDate! < earliestGeoImageCreationDate){
                    earliestGeoImageCreationDate = currentImage.creationDate!
                    earliestGeoImageModificationDate = currentImage.modificationDate!
                }
                
                if(currentImage.location!.distance(from: location) < radius){
                    //This one is valid to show
                    geoImagesInCurrentArea+=1;
                    geoCodedImagesIndexSet.insert(totalNumberOfImages-1);
                    
                    //check if this image has been previously seen
                    let stringID = getStringID(img: currentImage)
                    
                    //check the db 
                    let dbResult = db.query(sql:"SELECT * FROM SeenImages WHERE identifier='\(stringID)' ")
                    if(dbResult.count == 0){
                      //  print("Img \(stringID) not present in DB. Adding it to unseen images too")
                        unseenGeoCodedImagesIndexSet.insert(totalNumberOfImages-1)
                        unseenGeoImagesInCurrentArea+=1;
                    }else{
                       // print("Img \(stringID) was present in DB. It has been previously seen")
                    }

                }
            }
        }else{
            print("NOT AN IMAGE\n");
        }
        
    });
    
    if(geoImagesInCurrentArea > 0){
        nearbyImages = allPhotos.objects(at: geoCodedImagesIndexSet);
    }else{
        nearbyImages = [] //Need to supply an empty array or the result is not good.
    }
    
    //the images that are nearby but that we haven't seen on the main view yet!
    if(unseenGeoImagesInCurrentArea > 0){
        unseenNearbyImages = allPhotos.objects(at: unseenGeoCodedImagesIndexSet);
    }else{
        unseenNearbyImages = [] //Need to supply an empty array or the result is not good.
    }
    
    let str = "Found "+String(totalNumberOfImages)+" photographs, "+String(numberOfGeocodedImages)+" were geocoded with "+String(geoImagesInCurrentArea)+" in the current area";
        print(str);
    
    
    //put all we found in the result object and send it off to the caller.
    let currentDate = Date()
    let result = PhotoManagerResult(obtainedAt: currentDate, imagesInLib: totalNumberOfImages, geoImagesInLib: numberOfGeocodedImages, numGeoImagesInRequestedArea: geoImagesInCurrentArea, numUnseenGeoImagesInRequestedArea: unseenGeoImagesInCurrentArea, geoImageAssetsInRequestedArea: nearbyImages, unseenGeoImagesInRequestedArea: unseenNearbyImages,
        earliestGeoImageCreationDate: earliestGeoImageCreationDate,
        earliestGeoImageModificationDate: earliestGeoImageModificationDate)
                    
    
    return result;// return the image search result
    
    }
    
    
    
    
    
}
