//
//  ImageGridHeaderCollectionReusableView.swift
//  Reveal
//
//  Created by David McGookin on 14/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import UIKit

class ImageGridHeaderCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var mYearLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UILookAndFeel.defaultTintColour()
        mYearLabel.textColor = UILookAndFeel.defaultTitleTextColour()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
          }
    
    
    

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    
}
