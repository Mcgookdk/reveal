//
//  StatusViewDelegate.swift
//  Reveal
//
//  Created by David McGookin on 02/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import Foundation
import CoreLocation



protocol StatusViewControllerDelegate: class{
    
    func lastLocationUpdate()->CLLocation;
    
    func lastPhotoSearchLocation()->CLLocation;
    
    func photoSearchResultsForLastLocation()->PhotoManagerResult;
    
    func lastNotificationTriggerTime()->Date
}
