//
//  PreferenceManagerViewController.swift
//  Reveal
//
//  Created by David McGookin on 03/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import UIKit

class PreferenceManagerViewController: UIViewController {

    @IBOutlet weak var mSearchRadiusSlider: UISlider!
    @IBOutlet weak var mSearchRadiusTextLabel: UILabel!
    
    @IBOutlet weak var mMaxNotificationsPerDaySlider: UISlider!
    @IBOutlet weak var mMaxNotificationsPerDayTextLabel: UILabel!
    
    @IBOutlet weak var mEarliestNotificationTimeButton: UIButton!
    @IBOutlet weak var mlatestnotificationTimeButton: UIButton!
    
    var mSearchAreaSize: NSNumber  = 0
    var mMaxNotificationsPerDay: NSNumber = 0
    
    var mEarliestFireTimeHours: NSNumber = 0
    var mLatestFireTimeHours: NSNumber = 0
    var mEarliestFireTimeMins: NSNumber = 0
    var mLatestFireTimeMins: NSNumber = 0
    
    var mViewAppearDate: Date = Date()
    
    
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        navigationItem.title = "Preferences";
        //Need these to stop things disappearing behind the navigation bar and tab bar if they exist
        edgesForExtendedLayout = [];//stops the Top of the view going under the navigation bar
        extendedLayoutIncludesOpaqueBars = true;
        
              //add the refresh button to the toolbar
    
        let refreshBarButtonItem = UIBarButtonItem(title:"Dismiss", style: UIBarButtonItemStyle.plain, target: self, action: #selector(dismissViewController))
        self.navigationItem.setRightBarButton(refreshBarButtonItem, animated: true)
        
        // Do any additional setup after loading the view.
        //Access the prefs file and setup the UI
        mMaxNotificationsPerDay = (PlistManager.sharedInstance.getValueForKey(key: "NotificationsPerDay") as? NSNumber)!
        mMaxNotificationsPerDaySlider.setValue(mMaxNotificationsPerDay as! Float, animated: false)
        mMaxNotificationsPerDayTextLabel.text = "\(mMaxNotificationsPerDay)"
        
        
        
        //Access the prefs file and setup the UI
        mSearchAreaSize = (PlistManager.sharedInstance.getValueForKey(key: "SearchAreaSize") as? NSNumber)!
        mSearchRadiusSlider.setValue(mSearchAreaSize as! Float, animated: false)
        mSearchRadiusTextLabel.text = "\(mSearchAreaSize)m"
        
        
                //set the min and max fire time
        
        mEarliestFireTimeHours = (PlistManager.sharedInstance.getValueForKey(key: "EarliestNotificationTimeHours") as? NSNumber)!
    
        mLatestFireTimeHours = (PlistManager.sharedInstance.getValueForKey(key: "LatestNotificationTimeHours") as? NSNumber)!
    
        
        mEarliestFireTimeMins = (PlistManager.sharedInstance.getValueForKey(key: "EarliestNotificationTimeMinutes") as? NSNumber)!
        
        mLatestFireTimeMins = (PlistManager.sharedInstance.getValueForKey(key: "LatestNotificationTimeMinutes") as? NSNumber)!
    
        setMinAndMaxFireTimeLabels() //Its complicated so there is a helper function!
    
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        let blurEffect = UIBlurEffect(style: .light)//.extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        
        self.view.insertSubview(blurEffectView, at: 0)
        
        
        
        EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_OPENED_PREFERENCE_VIEW, description: "User Opened Preference View")
        mViewAppearDate = Date()
    }
    
    func setMinAndMaxFireTimeLabels(){
        
        let currentDate = Date()
        let calendar = Calendar.current //get the local timezone
        
        let earliestTime = calendar.date(bySettingHour: Int(mEarliestFireTimeHours), minute: Int(mEarliestFireTimeMins), second: 0, of: currentDate)
        let latestTime = calendar.date(bySettingHour: Int(mLatestFireTimeHours), minute: Int(mLatestFireTimeMins), second: 0, of: currentDate)
        
         //update the date taken label
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.none
        formatter.timeStyle = DateFormatter.Style.short
        mEarliestNotificationTimeButton.setTitle(formatter.string(from:earliestTime!), for: .normal)
        mlatestnotificationTimeButton.setTitle(formatter.string(from:latestTime!), for: .normal)
    
    }

    func dismissViewController(){
        
        //check the prefs if any have changed
        if ((PlistManager.sharedInstance.getValueForKey(key: "SearchAreaSize") as? NSNumber) != mSearchAreaSize){
            EventLogger.sharedInstance.LogEvent(code: EventLogger.PREFERENCE_VALUE_CHANGED, description: "Search Area Size Changed from \(PlistManager.sharedInstance.getValueForKey(key: "SearchAreaSize") as! NSNumber) to \(mSearchAreaSize)")
            
            PlistManager.sharedInstance.saveValue(value: mSearchAreaSize, forKey: "SearchAreaSize")
        }
        //check the prefs if any have changed
        if ((PlistManager.sharedInstance.getValueForKey(key: "NotificationsPerDay") as? NSNumber) != mMaxNotificationsPerDay){
            EventLogger.sharedInstance.LogEvent(code: EventLogger.PREFERENCE_VALUE_CHANGED, description: "Max Notifications Per Day Changed From \(PlistManager.sharedInstance.getValueForKey(key: "NotificationsPerDay") as! NSNumber) to \(mMaxNotificationsPerDay)")
            PlistManager.sharedInstance.saveValue(value: mMaxNotificationsPerDay, forKey: "NotificationsPerDay")
        }
        
        //check min and max fire time
        //check the prefs if any have changed
        if ((PlistManager.sharedInstance.getValueForKey(key: "EarliestNotificationTimeHours") as? NSNumber) != mEarliestFireTimeHours){
            EventLogger.sharedInstance.LogEvent(code: EventLogger.PREFERENCE_VALUE_CHANGED, description: "EarliestNotificationTimeHours Changed From \(PlistManager.sharedInstance.getValueForKey(key: "EarliestNotificationTimeHours") as! NSNumber) to \(mEarliestFireTimeHours)")
            PlistManager.sharedInstance.saveValue(value: mEarliestFireTimeHours, forKey: "EarliestNotificationTimeHours")
        }
        
        if ((PlistManager.sharedInstance.getValueForKey(key: "EarliestNotificationTimeMinutes") as? NSNumber) != mEarliestFireTimeMins){
            EventLogger.sharedInstance.LogEvent(code: EventLogger.PREFERENCE_VALUE_CHANGED, description: "EarliestNotificationTimeMinutes Changed From \(PlistManager.sharedInstance.getValueForKey(key: "EarliestNotificationTimeMinutes") as! NSNumber) to \(mEarliestFireTimeMins)")
            PlistManager.sharedInstance.saveValue(value: mEarliestFireTimeMins, forKey: "EarliestNotificationTimeMinutes")
        }
        
        if ((PlistManager.sharedInstance.getValueForKey(key: "LatestNotificationTimeHours") as? NSNumber) != mLatestFireTimeHours){
            EventLogger.sharedInstance.LogEvent(code: EventLogger.PREFERENCE_VALUE_CHANGED, description: "LatestNotificationTimeHours Changed From \(PlistManager.sharedInstance.getValueForKey(key: "LatestNotificationTimeHours") as! NSNumber) to \(mLatestFireTimeHours)")
            PlistManager.sharedInstance.saveValue(value: mLatestFireTimeHours, forKey: "LatestNotificationTimeHours")
        }
        
        if ((PlistManager.sharedInstance.getValueForKey(key: "LatestNotificationTimeMinutes") as? NSNumber) != mLatestFireTimeMins){
            EventLogger.sharedInstance.LogEvent(code: EventLogger.PREFERENCE_VALUE_CHANGED, description: "LatestNotificationTimeMinutes Changed From \(PlistManager.sharedInstance.getValueForKey(key: "LatestNotificationTimeMinutes") as! NSNumber) to \(mLatestFireTimeMins)")
            PlistManager.sharedInstance.saveValue(value: mLatestFireTimeMins, forKey: "LatestNotificationTimeMinutes")
        }
        
        var viewOnScreenTimeSeconds:Double = 0.0
        viewOnScreenTimeSeconds = Date().timeIntervalSince(mViewAppearDate)
        EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_DISMISSED_PREFERENCE_VIEW, description: "User Dismissed Preference View, \(viewOnScreenTimeSeconds)")
        
        self.dismiss(animated: true)
    }
    
    @IBAction func setEarliestNotificationTime(_ sender: Any) {
        print("set earliest notificationtime")
        let calendar = Calendar.current //get the local timezone
        let currentDate = Date()
       // var dateComponents = DateComponents()
        
        let existingDate = calendar.date(bySettingHour: Int(mEarliestFireTimeHours), minute: Int(mEarliestFireTimeMins), second: 0, of: currentDate)
        let earliestPossibleDate = calendar.date(bySettingHour: Int(0), minute: Int(0), second: 0, of: currentDate)
        let latestPossibleDate = calendar.date(bySettingHour: Int(mLatestFireTimeHours), minute: Int(mLatestFireTimeMins), second: 0, of: currentDate)
        
        
        DatePickerDialog().show("Earliest Notification", doneButtonTitle: "Set", cancelButtonTitle: "Cancel", defaultDate: existingDate!, minimumDate: earliestPossibleDate, maximumDate: latestPossibleDate, datePickerMode: .time) { (date) in
            if let dt = date {
                
                    self.mEarliestFireTimeHours =  ((calendar.component(.hour, from: dt)) as NSNumber)
                    self.mEarliestFireTimeMins =  ((calendar.component(.minute, from: dt)) as NSNumber)
                    self.setMinAndMaxFireTimeLabels()
                
            }
        }
        
    }
    
    
    @IBAction func setLatestNotificationtime(_ sender: Any) {
              print("set Latest notificationtime")
        
        let calendar = Calendar.current //get the local timezone
        let currentDate = Date()
       
        
        let existingDate = calendar.date(bySettingHour: Int(mLatestFireTimeHours), minute: Int(mLatestFireTimeMins), second: 0, of: currentDate)
        let earliestPossibleDate = calendar.date(bySettingHour: Int(mEarliestFireTimeHours), minute: Int(mEarliestFireTimeMins), second: 0, of: currentDate)
        let latestPossibleDate = calendar.date(bySettingHour: Int(23), minute: Int(59), second: 0, of: currentDate)
        
        
        DatePickerDialog().show("Latest Notification", doneButtonTitle: "Set", cancelButtonTitle: "Cancel", defaultDate: existingDate!, minimumDate: earliestPossibleDate, maximumDate: latestPossibleDate, datePickerMode: .time) { (date) in
            if let dt = date {
                
                //Because we are validating with latest possible time, we don't need to validate this here
              
                
                self.mLatestFireTimeHours =  ((calendar.component(.hour, from: dt)) as NSNumber)
                self.mLatestFireTimeMins =  ((calendar.component(.minute, from: dt)) as NSNumber)
                self.setMinAndMaxFireTimeLabels()
                
            }
        }
        
    }

    
    
    @IBAction func maxNotificationsSliderMoved(_ sender: Any) {
        mMaxNotificationsPerDay = Int(mMaxNotificationsPerDaySlider.value) as NSNumber;
        mMaxNotificationsPerDayTextLabel.text = "\(mMaxNotificationsPerDay)"
    }
    @IBAction func searchRadiusSliderMoved(_ sender: Any) {
        mSearchAreaSize = Int(mSearchRadiusSlider.value) as NSNumber;
        mSearchRadiusTextLabel.text = "\(mSearchAreaSize)m"
        
    }
    @IBAction func resetPreviouslySeenImages(_ sender: Any) {
          print("reseting seen images ")
        PhotoManager.resetSeenImages()
    }
}
