//
//  PhotoNotificationManagerDelegate.swift
//  Reveal
//
//  Created by David McGookin on 06/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import Foundation




protocol PhotoNotificationManagerDelegate: class{
    

    func appTriggeredFromNotification(withID:  String);
    

}
