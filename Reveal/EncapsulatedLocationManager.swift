//
//  EncapsulatedLocationManager.swift
//  Reveal
//
// An encapuslation of Core Location Manger to allow reuse and quick setup
//
//
//  Created by David McGookin on 28/02/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import Foundation
import CoreLocation

class EncapsulatedLocationManager: NSObject, CLLocationManagerDelegate{
   
    
    var mLocationManager: CLLocationManager!
    var mLastKnownLocation: CLLocation!
    
    //the delegate
    var mDelegate: EncapsulatedLocationManagerDelegate!
    
    
    // initialisation methods
    var mAccuracy: CLLocationAccuracy!
    var mMinDistance: Double!
    var mShouldMonitorSigChange: Bool = false;
    var mShouldSupportBackgroundUpdates: Bool = false;
    
    
    
    init(accuracy: CLLocationAccuracy, minDistance: Double, monitorSigChange: Bool, monitorBackgroundUpdates: Bool, delegate: EncapsulatedLocationManagerDelegate){
        super.init();
        mAccuracy = accuracy;
        mMinDistance = minDistance;
        mShouldMonitorSigChange = monitorSigChange;
        mShouldSupportBackgroundUpdates = monitorBackgroundUpdates;
        mDelegate = delegate;
        
        if(checkLocationServices()){
            print("have location services at least requested");
            mLocationManager.delegate = self;
            mLocationManager.desiredAccuracy = mAccuracy;
            mLocationManager.distanceFilter = mMinDistance;
            mLocationManager.allowsBackgroundLocationUpdates = mShouldSupportBackgroundUpdates;
            mLocationManager.pausesLocationUpdatesAutomatically = false; //This could be very bad. But if we change the location accuracy a lot when in background it is prob ok
            mLocationManager.activityType = CLActivityType.otherNavigation;
            
            mLastKnownLocation = CLLocation(latitude: 55.874045, longitude: -4.291979); //Lilybank gardens glasgow as default!
            }else{ //only returns false if location services are denied!
            
            print("location updates failed");
          
        }
        
        
        
    }
    
    func checkLocationAuthorisationStatus() -> CLAuthorizationStatus{
        return CLLocationManager.authorizationStatus();
        
    }
    
    //start updating locations
    func startLocationUpdates() -> Bool{
        if((mLocationManager) != nil){
            mLocationManager.startUpdatingLocation();
            if(mShouldMonitorSigChange){
                mLocationManager.startMonitoringSignificantLocationChanges();
            }
            return true;
        }else{
            print ("Location manager doesn't exist");
            return false;
        }
    }
    
    func pauseLocationUpdates(){
        AppDelegate.dumpIt(text: "ELM: Paused Location Updates")
        mLocationManager.stopUpdatingLocation()
    }
    
    func resumeLocationUpdates(){
        AppDelegate.dumpIt(text: "ELM: Resumed Location Updates")
        mLocationManager.startUpdatingLocation()
    }
    
    func backgroundLocationUpdates(){
        AppDelegate.dumpIt(text: "ELM: Backgrounding location updates")
        mLocationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers; //set this to be very far for location updates
        mDelegate.encapsulatedLocationManagerDidBackgroundLocationUpdates( mLocationManager)
        
        
        
    }
    
    func isLocationForeground() -> Bool{
        return mLocationManager.desiredAccuracy != kCLLocationAccuracyThreeKilometers; //if true then not foreground
    }
    
    func foregroundLocationUpdates(){
        AppDelegate.dumpIt(text: "ELM: Foregrounding location updates")
        mLocationManager.desiredAccuracy = mAccuracy;
        mDelegate.encapsulatedLocationManagerDidForegroundLocationUpdates(mLocationManager)
    }
   
    func applyExitGeoFence(location: CLLocation, radius: Double) -> String{
        let currentTime = Date()
        let identifier = "REGION_ID_\(currentTime.timeIntervalSince1970)" //this is the same as a we only ever want one region available
        let region = CLCircularRegion(center: location.coordinate, radius: radius, identifier: identifier)
        region.notifyOnExit = true
        mLocationManager.startMonitoring(for: region)
        self.dumpCurrentRegions() //DEBUG to check things are getting cleared and we arent leaking
        return identifier
    }
    
    //stop location updates
    func stopLocationUpdates(){
        if(mLocationManager != nil){
            mLocationManager.stopUpdatingLocation();
            if(mShouldMonitorSigChange){
                mLocationManager.stopMonitoringSignificantLocationChanges();
            }
        }
    }
    
    //check our location services are ok and init the location manager
    func checkLocationServices() -> Bool{
        
        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied){
            
            print("user denied access to location services");
            mDelegate.encapsulatedLocationMangerAuthorisationDidFail(status: CLLocationManager.authorizationStatus())
            return false;
        }else if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined){
            mLocationManager = CLLocationManager();
            mLocationManager.requestAlwaysAuthorization(); //request the authorisation //We might not always use this, but we always request it
            print("Authorisation status unknown");
        }else{
            mLocationManager = CLLocationManager();
        }
        
        return true; // We are not denied in using location so we can return true ok
    }
    
    func getLocation() -> CLLocation{
      return   mLocationManager.location!
    }
    
    //CLLocationManagerDelegates
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status:
        CLAuthorizationStatus){
        
        print("LOCATION MANAGER PREFS CHANGED");
        
        if ((status == CLAuthorizationStatus.denied) || (status == CLAuthorizationStatus.restricted)){
              mDelegate.encapsulatedLocationMangerAuthorisationDidFail(status: status)
        }
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        let str = "ELM: DID pause location updates (LMD) ++++ PROB BY IOS"
        AppDelegate.dumpIt(text: str)
        print(str)
       // mDelegate.encapsulatedLocationManagerDidPauseLocationUpdates(manager)
    }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        let str = "ELM: DID resume location updates (LMD) ++++ PROB BY IOS"
        AppDelegate.dumpIt(text: str)
        print(str)
       
       // mDelegate.encapsulatedLocationManagerDidResumeLocationUpdates(manager)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        print("ELM: New Location Update: \(locations[0])");
        mDelegate.encapsulatedLocationManagerDidUpdateLocations(locations: locations)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      //  mDelegate.encapsulatedLocationManager(manager, didFailWithError: error)
        print( "LOCATION MANAGER FAILURE");
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
    print("Monitoring failed for region with identifier: \(region!.identifier) and error \(error)")
        print("List of Monitored Regions\(manager.monitoredRegions.count):   \(manager.monitoredRegions)");
       AppDelegate.dumpIt(text: "Monitoring Region Failure have \(manager.monitoredRegions.count) regions")
        
        //DEBUG INCASE this error is called, we probably have 20 regions set for monitoring and have tried to add more
        //This should never really happen and if it does, these lines can clear the problem
        //for region in manager.monitoredRegions {
         //             manager.stopMonitoring(for: region)
           //             print ("UNMONITORING region")
        //}
    }
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("User did enter region \(region.identifier)")
            }
    
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
         print("User did exit region \(region.identifier)")
        manager.stopMonitoring(for: region);// We remove the region that was registered for monitoring automatically
        mDelegate.encapsulatedLocationManagerUserDidExitRegion(id: region.identifier)
    }
    
    
    func dumpCurrentRegions(){
        AppDelegate.dumpIt(text: "ELM: Dump of current monitored regions \(mLocationManager.monitoredRegions.count) regions")
    }
    
}
