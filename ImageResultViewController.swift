//
//  ImageResultViewController.swift
//  Reveal
//
//  Created by David McGookin on 20/02/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import UIKit
import MapKit
import Photos

class  ImageResultViewController: UIViewController, UIGestureRecognizerDelegate, PHPhotoLibraryChangeObserver {

    let NO_VALID_IMAGE_INDEX = -1
    var mCurrentPhotoResult: PhotoManagerResult? = nil //holds teh current results from the last search query
    var mImageIndexOfCurrentDisplayedImage: Int = -1 //the index of the images in the result we are currently viewing onscreen
    
    var mImageResultGridViewControllerIsActive: Bool = false //used to stop us double sending events
    var mTapLabelShouldBePresented: Bool = true
    
    var mFavoriteBarButtonItem: UIBarButtonItem? = nil //The favorite non-favorite bar button item
    
    @IBOutlet weak var mImageTakenLabelBackgroundView: UIView!
    
  //  @IBOutlet weak var mFavoriteImageToolbarItem: UIBarButtonItem!
   // @IBOutlet weak var mToolbar: UIToolbar!
    @IBOutlet weak var mShowMoreImagesView: UIView!
    @IBOutlet weak var mInfoView: UIView!
    @IBOutlet weak var mShowMoreImagesButton: UIButton!
    @IBOutlet weak var mImageTakenLabel: UILabel!
    @IBOutlet weak var mImageView: UIImageView!
  //  @IBOutlet weak var mImageMapView: MKMapView!
    
    @IBOutlet weak var mNoImagesnearbyExplanationTextLabel: UILabel! //This is on the top and should be presented removed based on the
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mImageResultGridViewControllerIsActive = false
        
        if(mImageIndexOfCurrentDisplayedImage != NO_VALID_IMAGE_INDEX){
            let imageToDisplay = mCurrentPhotoResult?.unseenGeoImagesInRequestedArea()[mImageIndexOfCurrentDisplayedImage];
            PhotoManager.setImageAsPreviouslySeen(img: imageToDisplay!)
            print ("Setting Image as previously seen")
            
            
            
        }
        
    }
//    @IBAction func userSelectedToolbarFavorite(_ sender: Any) {
  //      self.toggleAsFavoriteImage()
   //
  //  }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        self.mInfoView.alpha = 0.0
        
        //setup our tabbar item
        tabBarItem = UITabBarItem(title: "Image View", image: UIImage.init(named: "photo_image"), selectedImage: nil);
        navigationItem.title = "Reveal";
        //Need these to stop things disappearing behind the navigation bar and tab bar if they exist
        edgesForExtendedLayout = [];//stops the Top of the view going under the navigation bar
        extendedLayoutIncludesOpaqueBars = true;
        
            //We create this here but don't add it until later!
            let favImg = UIImage(named: "favorite_image_icon")
            mFavoriteBarButtonItem = UIBarButtonItem(image: favImg, style: UIBarButtonItemStyle.plain, target: self, action: #selector(toggleAsFavoriteImage))
        
        
        
        
        let tapGestureRecogniser = UITapGestureRecognizer(target: self, action:#selector(ImageResultViewController.tappedImage(sender:)))
        tapGestureRecogniser.delegate = self
        mImageView.addGestureRecognizer(tapGestureRecogniser)
        mImageView.isUserInteractionEnabled = true
        
        
        //add the refresh button to the toolbar
        let img = UIImage(named: "refresh_icon");
        let refreshBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(manuallyRefreshItems))
        
        let img2 = UIImage(named: "prefs_bar_icon")
        let prefsBarButtonItem = UIBarButtonItem(image: img2, style: UIBarButtonItemStyle.plain, target: self, action: #selector(presentPreferenceController))

    
        //  UIBarButtonItem(image:UIImage.init("refresh_icon"), target: self, action: #selector(manuallyRefreshItems))
        //navigationItem.setRightBarButtonItems([prefsBarButtonItem], animated: true)
        navigationItem.setLeftBarButtonItems([refreshBarButtonItem, prefsBarButtonItem], animated: true)
      //  navigationItem.setRightBarButton(refreshBarButtonItem, animated: true)
        
         PHPhotoLibrary.shared().register(self)
        
        resetUI(); //set the UI to a default state
        
    }
    
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        
        if(mTapLabelShouldBePresented){
            fadeTapLabelInOut()
        }
    }

    
    func tappedImage(sender: UITapGestureRecognizer){
        
        let scrollableImageViewController = LargeScrollableImageViewController();
        let imageToDisplay = mCurrentPhotoResult?.unseenGeoImagesInRequestedArea()[mImageIndexOfCurrentDisplayedImage];
        scrollableImageViewController.setAsset(asset: imageToDisplay!)
        
        EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_TAPPED_IMAGE_IN_MAIN_VIEW_CONTROLLER_FOR_LARGER_VIEW, description: "User requested large image with date \(imageToDisplay?.creationDate)")
        self.navigationController?.pushViewController(scrollableImageViewController, animated: true)
        

    
    }
    
    //internal function to rest the UI to a static form
    func resetUI(){
        mImageView.image = #imageLiteral(resourceName: "placeholder_image");
        mImageView.isUserInteractionEnabled = false; //do this so we don't try to load an image in large screen when none exists
        mImageTakenLabel.text = "";
        mImageTakenLabelBackgroundView.isHidden = true;
        mNoImagesnearbyExplanationTextLabel.isHidden = false;
      //  mShowMoreImagesButton.isHidden = true;
        mShowMoreImagesView.isHidden = true;
        mImageIndexOfCurrentDisplayedImage = NO_VALID_IMAGE_INDEX
        mTapLabelShouldBePresented = true
           self.mInfoView.alpha = 0.0 //Force this to be transparent Trying to debug something
        //  var currentNavItems = navigationItem.rightBarButtonItems
       // if(currentNavItems?.contains(mFavoriteBarButtonItem!))!{
         //   currentNavItems?.remove(at: (currentNavItems?.index(of: mFavoriteBarButtonItem!))!)
       // }
        navigationItem.setRightBarButtonItems([], animated: true)
        
   //     mFavoriteImageToolbarItem.isEnabled = false;
    }
    
    func updateUI(){
        
     //   mImageIndexOfCurrentDisplayedImage = 0
        mImageIndexOfCurrentDisplayedImage = Int(arc4random_uniform(UInt32((mCurrentPhotoResult?.numberOfUnseenGeoCodedImagesInRequestedArea())!))) //get a random unseen image
        print("Image is \(mImageIndexOfCurrentDisplayedImage)")
        mImageTakenLabelBackgroundView.isHidden = false
          mNoImagesnearbyExplanationTextLabel.isHidden = true;
        let imageToDisplay = mCurrentPhotoResult?.unseenGeoImagesInRequestedArea()[mImageIndexOfCurrentDisplayedImage];
        if(imageToDisplay != nil){
            setImage(imageAsset: imageToDisplay!)
            EventLogger.sharedInstance.LogEvent(code: EventLogger.SETTING_IMAGE_IN_MAIN_VIEW_AS, description: "Main View Image Set as \(PhotoManager.getStringID(img: imageToDisplay!))")
            
            
          
            updateFavoriteStatus(imageAsset: imageToDisplay!) //update if this is or is not a favorite
      //      var currentNavItems = navigationItem.rightBarButtonItems
        //    currentNavItems?.insert(mFavoriteBarButtonItem!, at: 0)
            navigationItem.setRightBarButtonItems([mFavoriteBarButtonItem!], animated: true)
            
          //  mFavoriteImageToolbarItem.isEnabled = true;
            
            //update the date taken label
            let formatter = DateFormatter()
            formatter.dateStyle = DateFormatter.Style.long
            formatter.timeStyle = DateFormatter.Style.none
            mImageTakenLabel.text = formatter.string(from: (imageToDisplay?.creationDate)!)
        }
        var imagesInArea = 0
        imagesInArea = (mCurrentPhotoResult?.numberOfGeoCodedImagesInRequestedArea())!
        if(imagesInArea > 1){
            mShowMoreImagesButton.setTitle("Show \(imagesInArea-1) More", for: .normal)// we take one off as we are already showing it!            
         //   mShowMoreImagesButton.isHidden = false;
            mShowMoreImagesView.isHidden = false;
            //are we onscreen?
            if (self.isViewLoaded && (self.view.window != nil)) {
                // viewController is visible
                PhotoManager.setImageAsPreviouslySeen(img: imageToDisplay!)
                print ("Setting Image as previously seen")
                
                //fade the tap thing in and out
                fadeTapLabelInOut()
            }
        }
        
        
    }
    
    
    func updateFavoriteStatus(imageAsset: PHAsset){
        //add the favorite button and set it correctly
        var img: UIImage!;
        if(imageAsset.isFavorite){
            img = UIImage(named: "favorite_image_icon")
            
        }else{
            img = UIImage(named: "not_favorite_image_icon")
        }
        mFavoriteBarButtonItem?.image = img
      //   mFavoriteImageToolbarItem?.image = img;
    }

    
    
    func setImage(imageAsset: PHAsset){
        
        let scale = UIScreen.main.scale;
        let cellSize = CGSize(width: 1000.0, height: 1000.0); //This is a bit of a hack, but seems to be a big enough image to encourage iOS to get a better quality image as we go //mImageView.bounds;//   CGSize(width: 500.0, height: 500.0);
        let thumbnailSize = CGSize(width: cellSize.width * scale, height: cellSize.height * scale)
        print("Media Type is : \(imageAsset.mediaType)");
        let requestOptions = PHImageRequestOptions();
            requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.opportunistic
            requestOptions.version = PHImageRequestOptionsVersion.current
        requestOptions.isSynchronous = false;
        requestOptions.isNetworkAccessAllowed = true; //need this or the image request might fail. AS iCloud can't get to network //we took it out and it didn't work, so we really need this
           requestOptions.progressHandler = { progress, _, _, _ in
                // Handler might not be called on the main queue, so re-dispatch for UI work.
                DispatchQueue.main.sync {
                  //  print("Image Loading Progress \(progress)")
                }
            }
            PHCachingImageManager().requestImage(for: imageAsset, targetSize: thumbnailSize, contentMode: .aspectFill, options: requestOptions, resultHandler: {(image, info)->Void in
            
            
            
            if(info != nil){
                print ("INFO HAS DATA \(info)");
                
                let error = info![PHImageErrorKey]; //info has to exist here since we check for it
                if(error == nil){
                   // print("NO ERROR")
                }else{
                    //print("THERE WAS AN ERROR: \(error)");
                    
                }
                
                
            }else{
               
                
            }
            //   let error = info?[PHImageErrorKey];
            // if(error == nil){
            //   print("NO ERROR")
            //}else{
            //  print("THERE WAS AN ERROR: \(error)");
            
            //}
            
           
            if(image != nil){
                self.mImageView.image = image;
                self.mImageView.isUserInteractionEnabled = true;
                
            
                
              //  UIView.animate(withDuration: 0.5, delay: 0.5, options: UIViewAnimationOptions.curveEaseOut, animations: {
             //       self.mImageView.alpha = 0.0
             //   }, completion: nil)//{ UIView.animate(withDuration: 0.5, delay: 0.5, options: UIViewAnimationOptions.curveEaseOut, animations: {
                   // self.mImageView.alpha = 1.0
                //}, completion: nil)})
            }else{
                
            }
        }
            
            
        )
    }
    
    //set unset image as favorite
    func toggleAsFavoriteImage(){
        let imageToDisplay = mCurrentPhotoResult?.unseenGeoImagesInRequestedArea()[mImageIndexOfCurrentDisplayedImage];
        var img: UIImage!
        if(imageToDisplay?.isFavorite)!{
            img = UIImage(named:"not_favorite_image_icon")
              EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_UNFAVORITED_PHOTO, description: "User un-favorited image with creation date \(imageToDisplay?.creationDate)")
            PhotoManager.setFavoriteStatus(asset:imageToDisplay!, status: false)
            
        }else{
            img = UIImage(named:"favorite_image_icon")
            EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_FAVORITED_PHOTO, description: "User favorited image with creation date \(imageToDisplay?.creationDate)")
            PhotoManager.setFavoriteStatus(asset: imageToDisplay!, status: true)
        }
        
      //  mFavoriteImageToolbarItem?.image = img;
        mFavoriteBarButtonItem?.image = img;
        
    }
    
    
    
    //Fades the tap to expand label in and out again
    func fadeTapLabelInOut(){
        mTapLabelShouldBePresented = false; //We only do this once
    /*   Debugged out, currently doesn't work well
         UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseIn, animations: {
            self.mInfoView.alpha = 1.0
        }, completion: {
            finished in
            
            if finished {
                //Once the label is completely invisible, set the text and fade it back in
                // self.yourLabel.text = "your Text "
                
                // Fade in
                UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseOut, animations: {
                    self.mInfoView.alpha = 0.0
                }, completion: nil)
            }
        })
 */
    }

    func updateImageResult(result: PhotoManagerResult){
        //nullify the UI
        resetUI()
        
        //set the variables
        mCurrentPhotoResult = result;
      // This is taken care of in resetUI  mImageIndexOfCurrentDisplayedImage = 0; //For the moment we default to the first image
    
        
        //check if there is a subview controller and dismiss it!
        //We pop back in the stack to this view contoller
        //we might want to do something smarter in the future.E.g. if the user is on the multiimage view and there are multiple images, just update those 
        //But! MAybe not such a big thing
        if (mImageResultGridViewControllerIsActive){
            self.navigationController?.popToViewController(self, animated: true)
            mImageResultGridViewControllerIsActive = false
        }
        //check if we have new images, if not then remove the geo images
        if (mCurrentPhotoResult?.numberOfUnseenGeoCodedImagesInRequestedArea() != 0){ //if there are no new geo images don't bother
           // AppDelegate.dumpIt(text: "There are images, but we've seen all the geo ones here so we stop showing them")
            //Update the UI
            updateUI();
            
        }else{
             EventLogger.sharedInstance.LogEvent(code: EventLogger.ALL_GEOPHOTOS_SEEN, description: "Found GeoPhotos but user has seen them all already")
        }
    }
    
    
    @IBAction func showMoreImages(_ sender: Any) {
         EventLogger.sharedInstance.LogEvent(code: EventLogger.USER_VIEWED_MORE_NEARBY_IMAGES, description: "User viewed the other \(String(describing: mCurrentPhotoResult?.numberOfGeoCodedImagesInRequestedArea())) images in the area")    //    let layoutParams = UICollectionViewFlowLayout();
      //  layoutParams.itemSize = CGSize(width: 500.0, height: 500.0)
        if(mImageResultGridViewControllerIsActive == false){
            let gridViewController = ImageResultGridViewController();//(collectionViewLayout: layoutParams)
            gridViewController.setImageResults(results: mCurrentPhotoResult!)
            mImageResultGridViewControllerIsActive = true;
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            navigationItem.backBarButtonItem = backItem //
            self.navigationController?.pushViewController(gridViewController, animated: true)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func manuallyRefreshItems(){
        
               print("refreshed");
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.manualUpdateImageResults();
    }
    func presentPreferenceController(){
        
        let navController = UINavigationController();
        navController.view.backgroundColor = UIColor.clear;
        // navController.navigationBar.barTintColor =  self.window?.tintColor
        navController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navController.navigationBar.tintColor = UIColor.white
        
        
        
        let prefManagerViewController = PreferenceManagerViewController()
        //prefManagerViewController.modalPresentationStyle = .overFullScreen;
        navController.navigationBar.barTintColor = UILookAndFeel.defaultTintColour()
        navController.modalPresentationStyle = .overFullScreen;
        navController.pushViewController(prefManagerViewController, animated: false);
        
        
        
        
        present(navController, animated: true, completion: nil)
        
       // print("presented preference controller");
        
    }
    
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
      //  print("PHOTO CHANGED")
        // Call might come on any background queue. Re-dispatch to the main queue to handle it.
        DispatchQueue.main.sync {
             if(mImageIndexOfCurrentDisplayedImage != NO_VALID_IMAGE_INDEX){
            var imageToDisplay = mCurrentPhotoResult?.unseenGeoImagesInRequestedArea()[mImageIndexOfCurrentDisplayedImage];
                
                                    // Check if there are changes to the asset we're displaying.
                    guard let details = changeInstance.changeDetails(for: imageToDisplay!) else { return }
                    
                    // Get the updated asset.
                   imageToDisplay = details.objectAfterChanges as! PHAsset
                    
                    // If the asset's content changed, update the image and stop any video playback.
                    if details.assetContentChanged {
                      //  print("CONTENT CHANGED")
                        // updateImage()
                        ///
                        //  playerLayer?.removeFromSuperlayer()
                        //   playerLayer = nil
                    }
                self.updateFavoriteStatus(imageAsset: imageToDisplay!)
            
            
    }

}
}
}
