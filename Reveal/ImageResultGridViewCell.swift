//
//  ImageResultGridViewCell.swift
//  Reveal
//
//  Acts as a thumbnail cell of images
//
//  Created by David McGookin on 01/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import UIKit

class ImageResultGridViewCell: UICollectionViewCell {

  
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    @IBOutlet weak var mFavoriteImage: UIImageView!
    
    var representedAssetIdentifier: String!
    
    var thumbnailImage: UIImage! {
        didSet {
            thumbnailImageView.image = thumbnailImage
        }
    }
    
    func setFavorite(fav: Bool){
        if(fav){
            mFavoriteImage.isHidden = false
        }else{
            mFavoriteImage.isHidden = true
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        thumbnailImageView.image = nil
            }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     
    }

      
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
