//
//  EventLogger.swift
//  Reveal
//
//  Base class for all the logging
//  Created by David McGookin on 31/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import Foundation



class EventLogger: NSObject{
    
    
    static private let LOG_TO_SERVER = true //True iff we should try logging to the server!
    
    /* lets for the codes*/
    let PHONE_STATS_EVENT_CODE = 1 //used only in the local logging
    
    //Noifications
   static let NOTIFICATION_ISSUED = 100
    static let NOTIFICATION_AUTODISMISSED = 101
   static  let NOTIFICATION_ACTED_ON = 102
    static let NOTIFICATION_ISSUED_WHEN_APP_WAS_ACTIVE = 103
    static let NOTIFICATION_REQUEST_REFUSED = 104; //we asked to fire a notification and were told no
    static let NOTIFICATION_REQUEST_EXCEEDS_MAX = 105;
    static let NOTIFICATION_REQUEST_OUTSIDE_VALID_HOURS = 106;
    
    //photo search results
    static let VALID_GEO_PHOTO_SEARCH_RESULT = 110 //We did a search and found valid geo photos
    static let NO_GEO_PHOTO_SEARCH_RESULT = 111 // We did a search and found no valid geo photos
    static let ALL_GEOPHOTOS_SEEN = 112 // There were valid geophotos, but the user has seen them all at this point
    
    static let USER_REQUESTED_MANUAL_PHOTO_SEARCH = 118 //If the user manually requested a search for photos
    static let USER_REQUESTED_MANUAL_PHOTO_SEARCH_WHEN_ALREADY_DOING_SO = 119 //If the user manually requested a search for photos but there was already one going on
    
    
    //General app use
    static let USER_OPENED_APP = 200
    static let USER_CLOSED_APP = 201
    
    static let APP_WAS_TERMINATED = 297
    static let APP_WAS_LAUNCHED = 298
    static let APP_WAS_LAUNCHED_IN_BACKGROUND  = 299
   static let USER_TAPPED_IMAGE_IN_MAIN_VIEW_CONTROLLER_FOR_LARGER_VIEW = 202 //user tapped a version to see the big zoomable version
    static let USER_TAPPED_IMAGE_IN_GRID_VIEW_CONTROLLER_FOR_LARGER_VIEW = 203 //user tapped image in grid to get bigger version
    
    static let PREFERENCE_VALUE_CHANGED = 400
    
    static let SETTING_IMAGE_IN_MAIN_VIEW_AS = 300
    
    //UI and app use
    
    //user opened and dismissed the details view with map
    static let USER_VIEWED_IMAGE_MAP_DETAILS = 204
    static let USER_DISMISSED_IMAGE_MAP_DETAILS = 205

    static let USER_VIEWED_MORE_NEARBY_IMAGES = 206 // user tapped the view more button
    
    static let USER_ZOOMED_LARGE_IMAGE = 207 //The user actually zoomed in on the
    
    
    static let USER_OPENED_SCROLLABLE_IMAGEVIEW = 210
    static let USER_DISMISSED_SCROLLABLE_IMAGEVIEW = 211
    
    static let USER_OPENED_GRID_VIEW = 215
    static let USER_DISMISSED_GRID_VIEW = 216
    
    static let USER_FAVORITED_PHOTO = 217
    static let USER_UNFAVORITED_PHOTO = 218
    
    static let USER_OPENED_PREFERENCE_VIEW = 250
    static let USER_DISMISSED_PREFERENCE_VIEW = 251
    

    static let sharedInstance = EventLogger()
    
    
    static let INVALID_USERNAME  = "INVALID USERNAME"
    
    var mUserID: String = INVALID_USERNAME
    
    var mServerLogger = ServerLogger()
   

  override init(){
    super.init();
        
        
}
    
    
    func setUserID(userID: String){
        mUserID = userID
        print("User Id Set as \(mUserID)")
        if(EventLogger.LOG_TO_SERVER){
            
            
            mServerLogger.register(username: mUserID)
        }
    }
    

    func logPhoneStats(model: String, oSString: String, images: Int, geoImages: Int){
        let logDate = Date()
        let dateStrFormatted = self.getFormattedDate(date: logDate)
        let timeSince1970Str = "\(logDate.timeIntervalSince1970)";
        
        self.logEventToFile(date: dateStrFormatted,  dateSince1970Str: timeSince1970Str, code: PHONE_STATS_EVENT_CODE, description: "model:\(model), OS Version:\(oSString), \(images), \(geoImages)\n") //We sub CSV this to get the images and geo Images in a seperate column (makes it easier to count them or process in the csv file). This is also why they aren't marked
                   if(EventLogger.LOG_TO_SERVER){
                    mServerLogger.logDevStat(formattedDate: dateStrFormatted, dateSince1970: timeSince1970Str, devID: model, osVersion: oSString, totalImages: images, geoImages: geoImages)
        }
    }

    func LogEvent(code: Int, description: String ){
        let logDate = Date()
        
        let dateStrFormatted = self.getFormattedDate(date: logDate)
        let timeSince1970Str = "\(logDate.timeIntervalSince1970)";
        
        self.logEventToFile(date: dateStrFormatted,  dateSince1970Str: timeSince1970Str, code: code, description: description);
       
        
        if(EventLogger.LOG_TO_SERVER){
            mServerLogger.logEvent(formattedDate: dateStrFormatted, dateSince1970: timeSince1970Str, eventCode: code, description: description);
        }
    }
    
    
    
    
    func flushServerLogCache(){
        if(EventLogger.LOG_TO_SERVER){
        let msg = "Flushing the Cache"
        print (msg);
        AppDelegate.dumpIt(text: msg)
        
        if(!mServerLogger.loggedIntoServer() && (mUserID != EventLogger.INVALID_USERNAME)){
            mServerLogger.login(username: mUserID)
            let msg = "Trying to Login"
            print (msg);
            AppDelegate.dumpIt(text: msg)
        }else if(mUserID == EventLogger.INVALID_USERNAME){
            let msg = "Do not have user id yet abort"
                print (msg);
                AppDelegate.dumpIt(text: msg)
            return;
            }else{
            let msg = "Have valid server login"
            print (msg);
            AppDelegate.dumpIt(text: msg)
        }
        
        mServerLogger.flushServerLogCache();
        }
    }
    
    
    
    //Private functions to log events to files 
    //
    
    private let TEXT_LOG_FILENAME = "event_log.csv"
    
    
    private func getFormattedDate(date: Date) -> String{
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "dd-MM-yyyy'T' HH:mm:ss ZZZZZ"
        let formattedDate = formatter.string(from: date)
        print("DATE IS: \(formattedDate)")
        return formattedDate
    }

    /* Writes a csv log file to the app container of events */
    private func logEventToFile(date: String, dateSince1970Str: String, code: Int, description: String){
             //let dateStr = self.getFormattedDate(date: date)
        
        let contentToAppend = "\(dateSince1970Str),\(date),\(code),\(description)\n" //the line of entry
        let filePath = NSHomeDirectory() + "/Documents/" + TEXT_LOG_FILENAME
        
        //Check if file exists
        if let fileHandle = FileHandle(forWritingAtPath: filePath) {
            //Append to file
            fileHandle.seekToEndOfFile()
            fileHandle.write(contentToAppend.data(using: String.Encoding.utf8)!)
            fileHandle.closeFile();
        }
        else {
            //Create new file
            do {
                let headerString = "TimeStamp,Date,Event Code,Description\n".appending(contentToAppend) // create a header for the file and append the content to it
                try headerString.write(toFile: filePath, atomically: true, encoding: String.Encoding.utf8)
            } catch {
                print("Error creating \(filePath)")
            }
        }
    }
 
}
