//
//  NotificationFireManager.swift
//  Reveal
//
//  Created by David McGookin on 27/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import Foundation



class NotificationFireManager: NSObject{
    
    var mLastRequestDate = Date(timeIntervalSince1970: 0) //Starts in 1970, so we should always be later than it
    var mNumberOfValidFiresToday = 0 //Holds the number of times we have returned true for requestFireAuthorisation() today
 
    
    //The minimum value that must be crossed to allow a notification to be triggered.
    let MINIMUM_FIRE_THRESHOLD = 0.5
    
    
    
    
    
    //Based on our probability functions, returns true iff the notification should be fired. Otherwise false.
    func getFireAuthoristation() -> Bool{
        
         let maxNotificationsPerDay = (PlistManager.sharedInstance.getValueForKey(key: "NotificationsPerDay") as? Int)!
        
        //if we are over our allocated amount of notifications, then automatically return false
        if(mNumberOfValidFiresToday >= maxNotificationsPerDay){
            print ("NFM: Too many notifications today so don't fire")
            AppDelegate.dumpIt(text: "NFM: Too many notifications today so don't fire")
            EventLogger.sharedInstance.LogEvent(code: EventLogger.NOTIFICATION_REQUEST_EXCEEDS_MAX, description: "Refusing Notification Fire Permission - Too Many Notifications today, \(mNumberOfValidFiresToday), \(maxNotificationsPerDay)")
            return false
        }
        
        //check we are not outside of the valid timezones
        let currentDate = Date()
        let calendar = Calendar.current //get the local timezone
        
        //get the prefs for earliest and latest times and setup earliest and latest fire dates
        let earliestFireTimeHours = (PlistManager.sharedInstance.getValueForKey(key: "EarliestNotificationTimeHours") as? Int)!
        let latestFireTimeHours = (PlistManager.sharedInstance.getValueForKey(key: "LatestNotificationTimeHours") as? Int)!
       let earliestFireTimeMins = (PlistManager.sharedInstance.getValueForKey(key: "EarliestNotificationTimeMinutes") as? Int)!
       let latestFireTimeMins = (PlistManager.sharedInstance.getValueForKey(key: "LatestNotificationTimeMinutes") as? Int)!
        
        let earliestTime = calendar.date(bySettingHour: earliestFireTimeHours, minute: earliestFireTimeMins, second: 0, of: currentDate)
        let latestTime = calendar.date(bySettingHour: latestFireTimeHours, minute: latestFireTimeMins, second: 0, of: currentDate)
        
        
        if (!((earliestTime)! <= currentDate && currentDate <= latestTime!)){
            print ("NFM: We are outside out valid fire hours. Don't fire")
            EventLogger.sharedInstance.LogEvent(code: EventLogger.NOTIFICATION_REQUEST_OUTSIDE_VALID_HOURS, description: "Refusing Notification Fire Permission - Outside of current fire time, \(currentDate), \(earliestTime), \(latestTime)")
            AppDelegate.dumpIt(text: "NFM: We are outside out valid fire hours. Don't fire")
            return false
        }
        
        //Check if the day has increased (i.e. it is a new day)
        //If it is then reset the counters
        if(!calendar.isDateInToday(mLastRequestDate)){
            mNumberOfValidFiresToday = 0;
        }
        mLastRequestDate = currentDate //Update teh last request date here
        
        
        //we are simplifying this and now just look not to exceed the number and the times set
        //so if we get here, return true
        
        return true;
        
        
        //if we reached this point, then we can do the prob calc.
        var notifProb = 0.0
        var hourProb = 0.0
        
        notifProb = 1.0 - (Double(mNumberOfValidFiresToday)/Double(maxNotificationsPerDay))
       let  timeInRange = latestTime?.timeIntervalSince(earliestTime!)
       let  timeLeft =  latestTime?.timeIntervalSince(currentDate) //There is no future time interval, so we have to work this back a bit
        hourProb = 1.0 - (Double(timeLeft!)/Double(timeInRange!))
        
        
        var overallProb = (notifProb + hourProb)/2.0
        
        print("NFM: Prob Calc notification factor :\(notifProb) timeLeftFactor:\(hourProb) overall: \(overallProb)")
        AppDelegate.dumpIt(text: "NFM: Prob Calc notification factor :\(notifProb) timeLeftFactor:\(hourProb) overall: \(overallProb)")
        if(overallProb < MINIMUM_FIRE_THRESHOLD){
            overallProb = MINIMUM_FIRE_THRESHOLD
        }
        
        //Get a random number and do the calc!
        let randomNum = Double(arc4random_uniform(UINT32_MAX))/Double(UINT32_MAX) //get a random between 0..1
               print("NFM: Random is \(randomNum) with prob \(overallProb)")
                AppDelegate.dumpIt(text: "NFM: Random is \(randomNum) with prob \(overallProb)")
        if(randomNum < overallProb){ //we are trying to make overallProb higher as the day goes on or when the number of fires are
            mNumberOfValidFiresToday += 1
            print("NFM: NOTIFICATION SHOULD FIRE at time \(currentDate)")
            AppDelegate.dumpIt(text: "NFM: NOTIFICATION SHOULD FIRE at time \(currentDate)")
            return true
        }else{
            print("NFM: NOTIFICATION SHOULD NOT FIRE at time \(currentDate)")
            AppDelegate.dumpIt(text: "NFM: NOTIFICATION SHOULD NOT FIRE at time \(currentDate)")
            return false
        }

    }
}
