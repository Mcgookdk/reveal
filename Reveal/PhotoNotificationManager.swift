//
//  PhotoNotificationManager.swift
//  Reveal
//
//  Created by David McGookin on 02/03/2017.
//  Copyright © 2017 David McGookin. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit


class PhotoNotificationManager: NSObject, UNUserNotificationCenterDelegate{
    static let NOTIFICATION_POST_ERROR = "Notification Failed To Post"
    
    var mDelegate: PhotoNotificationManagerDelegate?
    
    
//    init(_delegate: PhotoNotificationManagerDelegate){
  //      super.init()
    //    mDelegate = _delegate
    //}
   
    func setDelegate(_delegate: PhotoNotificationManagerDelegate){
        mDelegate = _delegate
    }
    
     func authoriseNotifications(){
        //setup notification support
        let center = UNUserNotificationCenter.current()
        
        center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            // Enable or disable features based on authorization.
            print ("notifications were \(granted), \(error)");
            //we should do something based on granting or not the notifications
            
        }
        //To make the notification delegate stick around (must be a weak reference) we need to have it referenced here. If not, the delegates are never called.
        //notificationDelegate = NotificationDelegate();
        center.delegate = self; //NotificationDelegate();
    }
    
    func removeNotification(id: String){
       UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [id])
        UIApplication.shared.applicationIconBadgeNumber = 0 //set badge icon back to 0
    }

    func fireImageNearbyNotification(numberOfImages: Int) -> String{
    let center = UNUserNotificationCenter.current()
        var idString: String
    let content = UNMutableNotificationContent()
    content.title = NSString.localizedUserNotificationString(forKey: "Reveal", arguments: nil)
    content.body = NSString.localizedUserNotificationString(forKey: "\(numberOfImages) images you took are nearby. Launch Reveal to see them", arguments: nil)
    content.categoryIdentifier="show";
//  content.sound = UNNotificationSound.default();
        content.sound = UNNotificationSound.init(named: "WRHIMC3.aiff");//
        content.badge = numberOfImages as NSNumber;
    
    
    //UNNotificationSound.init(named: "Sosumi.aiff");//UNNotificationSound.default();
    
    //  COMMENTED OUT ACTIONS FOR THE NOTIFICATIOn. THESE DONT WORK AS THE DELEGATE IS NEVER CALLED
  //  let showAction = UNNotificationAction(identifier: "show", title: "Tell me more…", options: .foreground)
 //   let secondAction = UNNotificationAction(identifier: "NEW ACTION", title: "Fuck off Siri",   options: .//foreground);
    //configure actions
 //   let category = UNNotificationCategory(identifier: "BACKGROUND_NOTE", actions: [showAction], intentIdentifiers: [], options:.customDismissAction)
 //   center.setNotificationCategories([category]);
    
    //ViewController.dumpIt(text: "Firing Notification: " + "BACKGROUND_NOTE\(mBackgroundLocationUpdates) "+content.body);
    // Configure the trigger for a 7am wakeup.
    //let trigger = UNCalendarNotificationTrigger(dateMatching: todayDateComponents, repeats: false)
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1,
                                                    repeats: false)        // Create the request object.
        
        let fireTime = Date()
       idString = "NEARBY_IMAGE_NOTIFICATION\(fireTime.timeIntervalSince1970)"
        print(idString)
        
    let request = UNNotificationRequest(identifier: idString, content: content, trigger: trigger)
    
                // `default` is now a property, not a method call
    center.add(request) { (error : Error?) in
        if let theError = error {
            print(theError.localizedDescription)
            idString = PhotoNotificationManager.NOTIFICATION_POST_ERROR //if no scheduled notification return nil but you cant in swift so return an error string :(
        }else{
            print("notification was sheduled");
           
        }
        
    }
    
    return idString //returns the string ID
}
    
    //Notification delegate methods
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Update the app interface directly.
        print("App is running with notification");
       EventLogger.sharedInstance.LogEvent(code: EventLogger.NOTIFICATION_ISSUED_WHEN_APP_WAS_ACTIVE, description: "Tried to issue notification when app was running")
        
        // Play a sound.
       // completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        
        //The app was triggered or launched by the notification. Should clear it in the delegate!
        mDelegate?.appTriggeredFromNotification(withID: response.notification.request.identifier)
         EventLogger.sharedInstance.LogEvent(code: EventLogger.NOTIFICATION_ACTED_ON, description: "User accessed app from Nearby Image Notification") //in theory
        
        print("DONES");
        if response.notification.request.content.categoryIdentifier == "TIMER_EXPIRED" {
            // Handle the actions for the expired timer.
            if response.actionIdentifier == "SNOOZE_ACTION" {
                // Invalidate the old timer and create a new one. . .
            }
            else if response.actionIdentifier == "STOP_ACTION" {
                // Invalidate the timer. . .
            }
        }
        
        // Else handle actions for other notification types. . .
        
        completionHandler(); //must call the completion handlers here
    }
    
    
    
    
    
}
